#ifndef __PARSER_H__
#define __PARSER_H__

#include "..\genres.h"
#include <ctype.h>

#define EMPTY_CHAR " \t"

struct str_parserInfo
{
	char *text;
	long numlines;
	long *linebuffer;
	long length;
};

#ifdef __cplusplus
extern "C" {
#endif

char parser_error[256];

struct str_parserInfo *parser_loadFromFile( FILE *hFile, long fileSize );
struct str_parserInfo *parser_load( char *text );
void parser_unload(struct str_parserInfo *parserInfo );

char *parser_getLine (struct str_parserInfo *parserInfo, long idx );
long parser_getLinesCount(struct str_parserInfo *parserInfo );

char* extractParameters( char *fullInfo );

char* copy( char* dst, const char* src );
char* trim( char* str, const char* t );
long words_count( char *string);



#ifdef __cplusplus
}
#endif

#endif
