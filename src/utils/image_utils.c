#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "..\genres.h"
#include "image_utils.h"
#include "packer.h"

static FIBITMAP *dib;
static RGBQUAD *palette;

static int paddedWidth; //,paddedHeight; //adjusted width&height to be 8 pixels roundary

static int *tilesData;
static unsigned char *packedData;

int nbTiles;
short int nbTilesX;
short int nbTilesY;
int packedSize;



int getLine(FIBITMAP *sourceDIB, int x, int y, u8 palId)
{
	int ret = 0;

	unsigned char colorIndex, i;

	for (i = 0; i< 8; i++)
	{
		ret <<= 4;

		if ( FreeImage_GetPixelIndex(sourceDIB, x+i, y, &colorIndex) )
		{
			if ( (colorIndex > (palId*15))  && (colorIndex < ((palId+1)*16)) )
			{
				ret |= (colorIndex - (palId*16))&0xF;
			}
		}
	}

	return( ret );
}

u8 image_load_bitmap(FIBITMAP *bitmap)
{
	dib = bitmap;
	if ( FreeImage_GetColorsUsed(dib) == 0 )
	{
		printf("Hi colors bitmap not supported\n");
		return FALSE;
	}

	palette = FreeImage_GetPalette(dib);

	return TRUE;
	
}


u8 image_load(char *filename)
{
	FREE_IMAGE_FORMAT fif;
	if (dib != NULL)
		image_unload();

	fif = FreeImage_GetFileType(filename, 0);
	if(fif == FIF_UNKNOWN) {
		// no signature ?
		// try to guess the file format from the file extension
		fif = FreeImage_GetFIFFromFilename(filename);
	}

	if(fif == FIF_UNKNOWN)
	{
		printf("Unknown file format\n");
		return FALSE;
	}

	//if png, FreeImage_Load(fif, filename, PNG_IGNOREGAMMA);

	FIBITMAP *loadedBMP = FreeImage_Load(fif, filename, 0);
	if (loadedBMP == NULL)
	{
		printf("Can't open file %s\n", filename);
		return FALSE;
	}
	
	return image_load_bitmap(loadedBMP);
}

FIBITMAP *image_clone( int x, int y, int width, int height)
{
	if (dib == NULL)	return NULL;

	return FreeImage_Copy(dib, x, y, x+width, y+height);
}

u8 image_getColorUsed( )
{
	BYTE colorIndex;
	int x, y;
	int maxX, maxY;
	u8 maxColor;
	
	if (dib == NULL)	return 0;
	
	colorIndex = 0;
	maxColor = 0;
	maxX = FreeImage_GetWidth(dib);
	maxY = FreeImage_GetHeight(dib);
	
	for(y=0; y< maxY; y++)
	{
		for(x=0; x< maxX; x++)
		{
			FreeImage_GetPixelIndex(dib, x, y, &colorIndex);
			maxColor = max(maxColor, colorIndex);
		}
	}
	
	return (maxColor+1);
}

u8 image_switch_alpha(char alphaCode)
{
	BYTE alphaColor;
	if (dib == NULL)	return FALSE;

	//never forget DIB is flipped!
	switch (alphaCode)
	{
		case ALPHA_NONE:
			return TRUE;
		case ALPHA_TOPLEFT:
			FreeImage_GetPixelIndex(dib, 0, FreeImage_GetHeight(dib) - 1, &alphaColor);
			break;
		case ALPHA_BOTTOMRIGHT:
			FreeImage_GetPixelIndex(dib, FreeImage_GetWidth(dib) - 1, 0, &alphaColor);
			break;

		default:
			if (alphaCode < 0)	return FALSE;
			if (alphaCode > 15)	return FALSE;
			alphaColor = alphaCode;
	}

	image_switch_color(0, alphaColor);

	return TRUE;
}

u8 image_switch_color(u8 indexA, u8 indexB)
{
	RGBQUAD palTmp;
	if (palette == NULL)	return FALSE;

	FreeImage_SwapPaletteIndices(dib, &indexA, &indexB);
	//not working
	//FreeImage_SwapColors(dib, &palette[0], &palette[transparency], TRUE);
	palTmp.rgbBlue = palette[indexA].rgbBlue;
	palTmp.rgbGreen = palette[indexA].rgbGreen;
	palTmp.rgbRed = palette[indexA].rgbRed;

	palette[indexA].rgbBlue = palette[indexB].rgbBlue;
	palette[indexA].rgbGreen = palette[indexB].rgbGreen;
	palette[indexA].rgbRed = palette[indexB].rgbRed;

	palette[indexB].rgbBlue = palTmp.rgbBlue;
	palette[indexB].rgbGreen = palTmp.rgbGreen;
	palette[indexB].rgbRed = palTmp.rgbRed;

	return TRUE;
}

int image_make_tiles(FIBITMAP *sourceDIB, char algo, u8 palId)
{
	int i,j,k, y;

	if (sourceDIB == NULL)	return FALSE;

	//clean previous
	if (tilesData != NULL)
	{
		free(tilesData);
		tilesData = NULL;
	}

	if (packedData != NULL)
	{
		free(packedData);
		packedData = NULL;
	}

	// get 8-size
	paddedWidth = FreeImage_GetWidth(sourceDIB);
	if(paddedWidth%8)	paddedWidth = (1+(paddedWidth/8))*8;

	nbTilesX = (FreeImage_GetWidth(sourceDIB)/8) & 0xFFFF;
	if(FreeImage_GetWidth(sourceDIB)%8)	nbTilesX++;

	nbTilesY = (FreeImage_GetHeight(sourceDIB)/8) & 0xFFFF;
	if(FreeImage_GetHeight(sourceDIB)%8)	nbTilesY++;

	nbTiles = nbTilesX*nbTilesY;
	packedSize = 8*nbTiles*sizeof(int);

	// convert bitmap data in tiles data
	tilesData = (int *)	malloc(packedSize);
	if (tilesData == NULL)
	{
		printf("Can't allocate %dbytes for tiles", packedSize);
		//FreeImage_Unload(dib);
        return (FALSE);
	}
	memset(tilesData, 0, packedSize);

	for(j=0;j<nbTilesY;j++)
	{
		for(i=0;i<nbTilesX;i++)
		{
			for( k=0; k<8; k++)
			{
				y = FreeImage_GetHeight(sourceDIB)-(k+j*8)-1;
				if ( y >= 0 )
				{
					tilesData[ (i + j*nbTilesX)*8 + k] = getLine(sourceDIB, i*8, y, palId);
				}

			}
		}
	}


	if (algo == ALGO_RLE)
	{
		packedData = compressRLE( (unsigned char *) tilesData, &packedSize);
	}

    return(packedSize);
}



void image_write_tile( FILE *output, int tileIndex  )
{
	int	j,k;
	char	string[256];

	j = tileIndex << 3;
	for( k=0; k<8; k++)
	{
		if (tileIndex >= nbTiles)
			sprintf(string, "\tdc.l 0x%.8x\n", 0);
		else
			sprintf(string, "\tdc.l 0x%.8x\n", tilesData[j + k]);
		
		fwrite(string, strlen(string), 1, output);
	}

}

void image_write_tiles( FILE *output )
{
	int	j;
	char	string[256];

	for(j=0;j<nbTiles;j++)
	{
		sprintf(string, "; tile %d\n", j);
        fwrite(string, strlen(string), 1, output);

		image_write_tile(output, j);

		sprintf(string, "\n");
        fwrite(string, strlen(string), 1, output);
	}
}

void image_pack_tiles(char algorithm, u8 palId)
{
	image_make_tiles(dib, algorithm, palId);
}

void image_write_packed( FILE *output )
{
	int	j;
	char	string[256];

	if (packedData != NULL)
	{
		for(j=0;j<packedSize;j+=2)
		{
			sprintf(string, "\tdc.w\t0x%.2x%.2x\n", packedData[j], packedData[j+1]);
			fwrite(string, strlen(string), 1, output);
		}
	}
	else
	{
		image_write_tiles(output);
	}

}


void image_write_pal( FILE *output, u8 palId )
{
	unsigned char	j;
	char	string[256];

	if (dib == NULL)	return;
	if (palette == NULL)	return;

	for (j=0; j<16; j++)
	{
		sprintf(string, "\tdc.w 0x%x%x%x\n", (palette[j+ palId*16].rgbBlue>>5)<<1, (palette[j+ palId*16].rgbGreen>>5)<<1, (palette[j+ palId*16].rgbRed>>5)<<1);
		fwrite(string, strlen(string), 1, output);
	}
}


void image_write_box(FIBITMAP *clone, FILE *output, u8 fromCenter)
{
	//TODO handle fromCenter TRUE

	char	string[256];

	int width, height;
	int top, left, bottom, right;
	int  x,y;
	unsigned char *bits;
	unsigned char pix;

	if (clone == NULL)	return;

	top = left = bottom = right = -1;
	width = FreeImage_GetWidth(clone);
	height = FreeImage_GetHeight(clone);

	if(FreeImage_GetBPP(clone) == 8)
	{
		for( y = 0; y < height; y++)
		{
			bits = FreeImage_GetScanLine(clone, height-y-1);
			for (x= 0; x < width; x++)
			{
				pix = bits[x];
				if (pix != 0)
				{
					if (top == -1)
						top = y;

					if (left == -1)
						left = x;
					else
						left = min(left, x);

					right = max(x, right);
					bottom = max(y, bottom);
				}
			}
		}
	}
	else if(FreeImage_GetBPP(clone) == 4)
	{
		for( y = 0; y < height; y++)
		{
			bits = FreeImage_GetScanLine(clone, height-y-1);
			for (x= 0; x < width/2; x++)
			{
				pix = bits[x];
				if  ( pix&0xF0 )
				{
					if (top == -1)
						top = y;

					if (left == -1)
						left = x*2;
					else
						left = min(left, x*2);


					right = max( (x*2), right);
					bottom = max(y, bottom);
				}

				if ( pix&0x0F )
				{
					if (top == -1)
						top = y;
					if (left == -1)
						left = (x*2) + 1;
					else
						left = min(left, (x*2)+1);

					right = max( ((x*2)+1), right);
					bottom = max(y, bottom);
				}
			}
		}
	}

	if (top==-1) //empty tile
		top = left = bottom = right = 0;

	sprintf(string, "\tdc.b %d,%d,%d,%d ; top, left, bottom, right\n\n", top, left, bottom, right);
	fwrite(string, strlen(string), 1, output);
}


u8 image_load_tiles( char *filename, char alphaCode )
{
	if (!image_load(filename) )
		return FALSE;

	image_switch_alpha(alphaCode);

    return TRUE;
}

void image_unload()
{

	if (dib != NULL)
	{
		FreeImage_Unload(dib);
		dib = NULL;
	}
	if (palette != NULL)
		palette = NULL;

	if (packedData != NULL)
	{
		free(packedData);
		packedData = NULL;
	}

	if (tilesData != NULL)
	{
		free(tilesData);
		tilesData = NULL;
	}
}
