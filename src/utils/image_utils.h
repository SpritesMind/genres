#ifndef __IMAGE_UTILS_H__
#define __IMAGE_UTILS_H__

#define		ALPHA_NONE			-1
#define		ALPHA_TOPLEFT		-2
#define		ALPHA_BOTTOMRIGHT	-3

extern int nbTiles;
extern short int nbTilesX;
extern short int nbTilesY;
extern int packedSize;

#ifdef __cplusplus
extern "C" {
#endif

u8 image_load_bitmap(FIBITMAP *bitmap);
u8 image_load_tiles(char *filename, char alphaCode);
u8 image_load(char *filename);
FIBITMAP *image_clone( int x, int y, int width, int height);
u8 image_switch_color(u8 indexA, u8 indexB);
u8 image_switch_alpha(char alphaCode);
u8 image_getColorUsed( );
int image_make_tiles(FIBITMAP *clone, char algo, u8 palId);
void image_pack_tiles(char algo, u8 palId);
void image_write_tile(FILE *output, int tileIndex );
void image_write_packed(FILE *output);
void image_write_pal(FILE *output, u8 palId);
void image_write_box(FIBITMAP *clone, FILE *output, u8 fromCenter);
void image_unload();

#ifdef __cplusplus
}
#endif

#endif
