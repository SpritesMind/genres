#ifndef __SOUND_UTILS_H__
#define __SOUND_UTILS_H__


#include "..\genres.h"

#define		SND_FORMAT_PCM		1 //8 bit signed from 8 Khz up to 32 Khz
#define		SND_FORMAT_2ADPCM	2 //4 bits ADPCM at 22050 Hz
#define		SND_FORMAT_4PCM		3 //8 bit signed at 16 Khz, 256 bytes boundary (address and size)
#define		SND_FORMAT_MVS		4 //??, max 65536 byte
#define		SND_FORMAT_VGM		5 //8 bit signed (?), max 8Khz (?!)
#define		SND_FORMAT_EWF		6 //8 bits unsigned, at 10650Hz mono, with 0xFE highest possible value
#define		SND_FORMAT_XGM		7 //8 bits unsigned, at 14KHz

typedef struct dac_format
{
	char formatID;
	int defaultRate;
	const char *name;
	const char *sox_format;
	const char *help;

}dac_format;

extern dac_format formats[];


#ifdef __cplusplus
extern "C" {
#endif

u8 sound_init(char *inputFilename, char *outputFilename, const char *sox_format);
void sound_mono();
void sound_trim();
void sound_resample(int rate);
void sound_process();
void sound_unload();


#ifdef __cplusplus
}
#endif

#endif
