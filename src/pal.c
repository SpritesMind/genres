#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/image_utils.h"
#include "utils/parser.h"


void pal_convert( FILE *output, char *id  )
{
	char	string[256];

	//header
	sprintf(string, ";;;;;;;;;; PAL \n\t.align 2\n\t.globl %s\n%s:\n", id, id);
    fwrite(string, strlen(string), 1, output);


	image_write_pal( output, 0 );
}


////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 palExecute(char *info, FILE *output)
{
	char keyword[4]; //PAL\0
	char id[50];
	char file[MAX_PATH];
	int nbElem=0;


	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	if (nbElem < 3)
	{
		printf("Wrong PAL definition\n");
		palHelp();
		return FALSE;
	}

	info += (strlen(file) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //PAL
		printf("  id   : %s \n", id);
		printf("  file : %s \n", file);
	}

	if (!image_load(file))
	{
		printf("File not found\n");
		return FALSE;
	}
	pal_convert(output, id );

	image_unload();

	return TRUE;
}

void palHelp()
{
	printf("PAL lets you import and convert a pal from a 16 or 256 colors bitmap\n");
	printf("Only the first 16 colors are imported\n");
	printf("\nBasic usage:\n");
	printf("\tPAL id \"file.bmp\"\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tthe bitmap to extract pal from");
}
