#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/parser.h"
#include "utils/file_utils.h"


////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 dataExecute(char *info, FILE *output)
{
	char keyword[5]; //DATA\0
	char id[50];
	char filename[MAX_PATH];
	char	string[MAX_PATH];
	int nbElem = 0;

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", filename);
	if (nbElem < 3)
	{
		printf("Wrong DATA definition\n");
		dataHelp();
		return FALSE;
	}
	info += (strlen(filename) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //CONTROLS
		printf("  id   : %s \n", id);
		printf("  file : %s \n", filename);
	}

	if (!file_isValid(filename))    return FALSE;

	sprintf(string, ";;;;;;;;;; DATA \n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);
	sprintf(string, "\tINCBIN \"%s\"\n", filename);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\n\t.set %s_len, .-%s\n", id, id);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\n\t.align 2\n\t.globl %s_size\n%s_size:\n\tdc.l %s_len\n\n", id, id, id);
	fwrite(string, strlen(string), 1, output);

	return TRUE;
}

void dataHelp()
{
	printf("DATA import any raw/binary data\n");
	printf("similar to asm's \"INCBIN file\"\n");
	printf("\nBasic usage:\n");
	printf("\tDATA id \"file\"\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tbinary file to import\n\n");

	printf("\nExample:\n");
	printf("DATA echo_driver \"resource\\echo.bin\"");
}
