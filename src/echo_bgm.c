#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/file_utils.h"
#include "utils/echo_utils.h"
#include "utils/midi_utils.h"
#include "utils/parser.h"

struct options
{
	u8 FMDrive_enabled;
	u8 SPSG_enabled;
} echo_bgm_options;

struct duo{
	const char *name;
	int value;
};
static const struct duo mappingNames[NUM_CHAN]={
		{ "FM1"   , CHAN_FM1 },
		{ "FM2"   , CHAN_FM2 },
		{ "FM3"   , CHAN_FM3 },
		{ "FM4"   , CHAN_FM4 },
		{ "FM5"   , CHAN_FM5 },
		{ "FM6"   , CHAN_FM6 },
		{ "PSG1"  , CHAN_PSG1 },
		{ "PSG2"  , CHAN_PSG2 },
		{ "PSG3"  , CHAN_PSG3 },
		{ "PSG4"  , CHAN_PSG4 },
		{ "PSG4EX", CHAN_PSG4EX },
		{ "PCM"   , CHAN_PCM }
};

// MAPPING STUFF
static void handleChannelMapping(char *text)
{
	int items_read;
	int nbChar;
	int channelID;
	char echoName[7];
	int echoID;
	char *ptr = text;

	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &channelID, &nbChar);
	if (items_read != 1)
	{
		printf("unable to read MIDI channel ID from %s\n", text);
		return;
	}

	if (channelID < 1 || channelID > 16) {
		printf("You can't remap MIDI channel %d (1-16)\n", channelID);
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	//handle chanel code or index(?)
	echoID = CHAN_NONE;
	items_read = sscanf(ptr, "%s%n", echoName, &nbChar);
	if ( (items_read != 1) || (nbChar == 1) )
	{
		items_read = sscanf(ptr, "%d%n", &echoID, &nbChar);
		if (items_read != 1)
		{
			printf("Unable to read Echo channel ID from %s\n", text);
			return;
		}
	}
	else
	{
		int i;
		for (i = 0; i < NUM_CHAN-1; i++)
		{
			if ( strcmp( echoName, mappingNames[i].name ) == 0)
			{
				echoID = mappingNames[i].value;
				break;
			}
		}

		if (echoID == CHAN_NONE)
		{
			printf("Invalid Echo channel name : %s\n", echoName);
			printf("Valid names :\n");
			for (i = 0; i < NUM_CHAN; i++)
			{
				printf("%s, ", mappingNames[i].name);
			}
			printf("\n");
			return;
		}
	}


	if ((echoID < CHAN_FM1) || (echoID > CHAN_PCM))
	{
		printf("Couldn't map to Echo channel %d", echoID);
		return;
	}

	if ( (echoID == CHAN_PCM ) && (channelID != 10) )
	{
		printf("Echo channel %d (PCM) could only be remapped to MIDI channel 10\n", echoID);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	map_channel(channelID, echoID);
}
static void handleInstrMapping(char *text)
{
	int items_read;
	int nbChar;
	int voiceID;
	int echoID;
	int instr_type = 0;
	char *ptr = text;

	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &voiceID, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read MIDI instrument ID from %s\n", text);
		return;
	}

	if ((voiceID < 0) || (voiceID >= NUM_MIDIINSTR))
	{
		printf("MIDI instrument ID %d is invalid (0-%d)\n", voiceID, NUM_MIDIINSTR-1);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &echoID, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read Echo instrument ID from %s\n", text);
		return;
	}

	if ( (echoID < 0) || (echoID >= NUM_ECHOINSTR))
	{
		printf("Echo instrument ID %d is invalid\n", echoID);
		return;
	}
	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	if (!strncmp(ptr, "FM", strlen("FM")))
	{
		ptr += strlen("FM");
		instr_type = INSTR_FM;
	}
	else if (!strncmp(ptr, "PSG", strlen("PSG")))
	{
		ptr += strlen("PSG");
		instr_type = INSTR_PSG;
	}
	else
	{
		printf("Invalid instrument type (FM/PSG) : %s\n", ptr);
		return;
	}
	trim(ptr, EMPTY_CHAR);

	map_instrument(instr_type, voiceID, echoID, 0, 100);
}

static void handleDrumMapping(char *text)
{
	int items_read;
	int nbChar;
	int noteID;
	int echoID;
	int instr_type = 0;
	char *ptr = text;

	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &noteID, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read MIDI drum note ID from %s\n", text);
		return;
	}

	if ((noteID < 35) || (noteID > 81))
	{
		printf("MIDI note ID %d is invalid (35-81)\n", noteID);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &echoID, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read Echo instrument ID from %s\n", text);
		return;
	}

	if ((echoID < 0) || (echoID >= NUM_ECHOINSTR))
	{
		printf("Echo instrument ID %d is invalid\n", echoID);
		return;
	}
	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	instr_type = INSTR_PCM;

	map_instrument(instr_type, noteID, echoID, 0, 100);
}

static void handleLine(char *text)
{
	char *ptr = malloc( strlen(text)+1 );
	char *savePtr = ptr;

    memset(ptr, 0, strlen(text)+1);
    memcpy(ptr, text, strlen(text));


	if ( strncmp(ptr, "CHANNEL" , strlen("CHANNEL")) == 0 )
	{
		ptr += strlen("CHANNEL");
		handleChannelMapping(ptr);
	}
	else if(strncmp(ptr, "INSTRUMENT", strlen("INSTRUMENT")) == 0)
	{
		ptr += strlen("INSTRUMENT");
		handleInstrMapping(ptr);
	}
	else if(strncmp(ptr, "VOICE", strlen("VOICE")) == 0)
	{
		ptr += strlen("VOICE");
		handleInstrMapping(ptr);
	}
	else if (strncmp(ptr, "DRUM", strlen("DRUM")) == 0)
	{
		ptr += strlen("DRUM");
		handleDrumMapping(ptr);
	}


	free(savePtr);
	return;
}


u8 parseMapping(char *text)
{
	long i, nbLines;
	struct str_parserInfo *mappingParseInfo;

	mappingParseInfo = parser_load(text);
	if (mappingParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

    nbLines = parser_getLinesCount(mappingParseInfo);
	for (i = 0; i< nbLines; i++)
	{
		handleLine((char *)parser_getLine(mappingParseInfo, i));
	}

	parser_unload(mappingParseInfo);

	return TRUE;
}



static u8 processFile(FILE *output, char *id, char *filename)
{
	char outputFilename[MAX_PATH];
	char	string[MAX_PATH];
	char	*ext;

	ext = file_getExtension(_strlwr(filename));

	if (strncmp(ext, "esf", strlen("esf")) == 0)
	{
		strcpy(outputFilename, filename);
	}
	else
	{
		file_getNewFilename(filename, outputFilename, ".esf");

		if (verbose)
			printf("\nExport to %s\n\n***** MIDI START *****\n", outputFilename);

		read_midi(filename);

		if (verbose)
			printf("***** MIDI END *****\n");

		if (write_esf(outputFilename, 0) != ERR_NONE)	return FALSE;
	}

	sprintf(string, ";;;;;;;;;; ECHO_BGM\n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tINCBIN \"%s\"\n", outputFilename);
	fwrite(string, strlen(string), 1, output);

	return TRUE;
}

////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 echo_bgmExecute(char *info, FILE *output)
{
	char keywordx[9]; //ECHO_BGM\0
	char id[50];
	char file[MAX_PATH];
	char *parameters;
	char *option;
	int nbElem = 0;

	parameters = extractParameters(info);
	/*
	if (parameters == NULL)
	{
		printf("Error : MIDI mapping is mandatroy\n");
		echo_bgmHelp();
		return FALSE;
	}
	*/

	nbElem += sscanf(info, "%s", keywordx);
	info += strlen(keywordx);
	trim(info, EMPTY_CHAR);


	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	if (nbElem < 3)
	{
		printf("Wrong ECHO_BGM definition\n");
		echo_bgmHelp();
		return FALSE;
	}
	info += (strlen(file) + 2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);


	echo_bgm_options.FMDrive_enabled = FALSE;
	echo_bgm_options.SPSG_enabled = FALSE;

	if ( strlen(info) )
	{
		option = strstr(info, "FMDRIVE");
		if (option != NULL)
		{
			echo_bgm_options.FMDrive_enabled = TRUE;
		}

		option = strstr(info, "SPSG");
		if (option != NULL)
		{
			echo_bgm_options.SPSG_enabled = TRUE;
		}
	}

	if (verbose)
	{
		printf("\n%s found\n", keywordx);	 //ECHO_BGM
		printf("  id    : %s \n", id);
		printf("  song  : %s \n\n", file);
	}

	//init default
	reset_mapping();
	if (parameters != NULL)
		parseMapping(parameters);

	return processFile(output, id, file);
}

void echo_bgmHelp()
{
	printf("ECHO_BGM import (and convert to) Echo song format\n");
	printf("Support MIDI type 0/1, 16 channels max\n");
	printf("See ECHO_BANK for instruments define\n");
	printf("Based on awesome Sik's tools (http://tools.mdscene.net)\n");

	printf("\nBasic usage:\n");
	printf("\tECHO_BGM id \"file\"\n");
	//printf("\tECHO_BGM id \"file\" [option]\n");
	printf("\t{\n");
	printf("\t\t<mapping>\n");
	printf("\t\t...\n");
	printf("\t}\n");

	printf("\nwhere\n\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tsong to import (ESF or MIDI)\n");

	//printf("\n  Options (MIDI):\n");
	//printf("  - FMDRIVE\t(MIDI) enable support of MIDICC for AlyJames' FMDrive\n");
	//printf("  - SPSG\t\t(MIDI) enable support of AlyJames' SuperPSG\n");

	printf("\n  Mappings (MIDI only, mandatory):\n");
	printf("  - CHANNEL midi_chan echo_chan\t\tMIDI channel to ESF channel\n");
	printf("    	MIDI channel : 1 to 16\n");
	printf("    	ESF channel : FM1 to FM6, PSG1 to PSG4, PCM\n");
	printf("  - VOICE midi_voice echo_instr type\tMIDI voice to ECHO's instrument\n");
	printf("    	MIDI voice : 0 to %d\n", NUM_MIDIINSTR-1);
	printf("    	ECHO instr : 0 to %d\n", NUM_ECHOINSTR-1);
	printf("    	type       : FM or PSG (no PCM, see DRUM)\n");
	printf("  - DRUM midi_voice echo_instr\t\tlike VOICE but for DRUM\n");
	printf("    	DRUM voice : 35 to 81\n");
	printf("    	ECHO instr : 0 to %d (PCM)\n", NUM_ECHOINSTR-1);

	printf("\n\nExample (MIDI):\n");

	printf("ECHO_BGM midi_sample \"data/music/MIDI_sample.mid\"\n");
	printf("{\n");
	printf("\tCHANNEL 1 FM1\n");
	printf("\tDRUM 36 0\n");
	printf("\tDRUM 38 1\n");
	printf("\tINSTRUMENT 33 2 FM\n");
	printf("}");

}
