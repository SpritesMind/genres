#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mappy.h>

#include "genres.h"
#include "utils/packer.h"
#include "utils/parser.h"
#include "utils/image_utils.h"
#include "utils/file_utils.h"

#define MODE_TILE	0
#define MODE_CHUNK	1

#define MAPPY_LAYER_COUNT	100

struct stillBlocksOptions
{
	u8 packer;
	u8 exportPal;
	s16 count;
	u16 propertiesMask;
};

struct animatedBlocksOptions
{
	u8 packer;
	s16 count;
};

struct layerOptions
{
	u8 export;
	u8 packer;
};

struct options
{
	u8 mapMode; //0: tile, 1: chunk
	u8 force16;

	struct stillBlocksOptions exportStillBlocks;
	struct animatedBlocksOptions exportAnimatedBlocks;
	struct layerOptions exportLayers[MAPPY_LAYER_COUNT];
} map_options;

static u8	nbLayers;
static u16	nbTilePerBlock;

static HDRSTR *header;


static FIBITMAP *toBitmap(CHUNK *chunk)
{
	CHUNK *cmap;
	FIBITMAP *bitmap = NULL;
	short int colordepth;
	int pitch = 4;
	
	colordepth = header->mapdepth;
	if (colordepth == 8)
	{
		cmap =  mappy_getChunk( "CMAP" );
		if (cmap == NULL)
		{
			printf("error : color map not found.\n");
			return NULL;
		}
	}
	
		
	if (colordepth == 8)
	{
		pitch = 1;
	}
	else if (colordepth < 24)
	{
		pitch = 2;
	}
	else if (colordepth < 32)
	{
		pitch = 3;
	}
	
	bitmap = FreeImage_ConvertFromRawBitsEx(TRUE, chunk->data, FIT_BITMAP, header->mapblockwidth, header->mapblockheight*header->mapnumblockgfx, pitch*header->mapblockwidth, colordepth, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE);
	if (colordepth == 8)
	{
		RGBQUAD *pal = FreeImage_GetPalette(bitmap);
		int i;
		for (i = 0; i < 256; i++) {
			 pal[i].rgbRed 	= cmap->data[i*3 + 0];
			 pal[i].rgbGreen= cmap->data[i*3 + 1];
			 pal[i].rgbBlue = cmap->data[i*3 + 2];
		}
	}

	return bitmap;
}

void exportMap( char *id, FILE *output )
{
	int	chunkIdx;
	u16 blockToExport = 0;


	unsigned char	layeridx, layercount;

	int	j, x, y, z;
	char	string[256];
	
	blockToExport = 0; //if map_options.exportStillBlocks.count == -1
	if (map_options.exportStillBlocks.count == 0) 
		blockToExport = header->mapnumblockstr;
	else if (map_options.exportStillBlocks.count > 0) 
		blockToExport = map_options.exportStillBlocks.count;

	if (blockToExport) 
	{
		FIBITMAP *bitmap = NULL;
		CHUNK *rawChunk ;
		u8 nbColors;
		rawChunk = mappy_getChunk( "BGFX" );
		if (rawChunk == NULL)
		{
			printf("error : tiles (chunk BGFX) not found.\n");
			return;
		}
	
		//create a bitmap of width = chunk width, height = chunk height*nbChunk
		bitmap = toBitmap(rawChunk);
		image_load_bitmap(bitmap);
		
		nbColors = image_getColorUsed();
		if (nbColors == 0)
		{
			image_unload();
			printf("error : invalid color size.\n");
			return;
		}

		if (map_options.force16)
		{
			if (verbose)
			{
				printf("Reduce %d colors to 16\n", nbColors);
			}
								
			if (nbColors > 16)
			{
				FIBITMAP *bitmap16 = NULL;
				bitmap16 = FreeImage_ColorQuantizeEx( FreeImage_ConvertTo24Bits(bitmap), FIQ_WUQUANT, 16, 0, NULL);

				image_load_bitmap(bitmap16);
				image_switch_alpha(ALPHA_TOPLEFT); //since firt tile is empty, we can't be wrong here
													//and need to be done because ColorQuantizeEx mess with the pal order
													//since we make 8bit to 24 bit to 8bit :(
				//FreeImage_Save(FIF_BMP, bitmap16, "test_quantize.bmp", 0);
			}
		}
		else
		{
			if ( (verbose) && (nbColors > 16))
			{
				printf("Only first 16 colors of %d used\n", nbColors);
			}
		}
		
		image_pack_tiles(map_options.exportStillBlocks.packer, 0);
	}

	sprintf(string, ";;;;;;;;;; MAP \n\t.align 2\n\t.globl %s\n%s:\n", id, id);
    fwrite(string, strlen(string), 1, output);

	if (map_options.mapMode == MODE_TILE)
		sprintf(string, "\tdc.w\t%d, %d\t ; 8x8 tiles\n", header->mapwidth*header->mapblockwidth/8, header->mapheight*header->mapblockheight/8);
	else
		sprintf(string, "\tdc.w\t%d, %d\t ; chunk x chunk\n", header->mapwidth, header->mapheight);
    fwrite(string, strlen(string), 1, output);

	if (map_options.exportStillBlocks.exportPal)
	{
		sprintf(string, "\tdc.l\t%s_pal\n", id);
	}
	else
	{
		sprintf(string, "\tdc.l\t0\t; pal (disabled)\n");
	}
    fwrite(string, strlen(string), 1, output);

	
	sprintf(string, "\n");
    fwrite(string, strlen(string), 1, output);
	
	if (blockToExport)
	{
		if (map_options.mapMode == MODE_TILE)
			sprintf(string, "\tdc.w\t%d\t ; nb tiles\n", blockToExport*nbTilePerBlock);
		else
			sprintf(string, "\tdc.w\t%d\t ; nb chunk\n", blockToExport);
	}
	else
	{
		sprintf(string, "\tdc.w\t0\t ; nb tiles (disabled)\n");
	}
    fwrite(string, strlen(string), 1, output);

	
	if (blockToExport)
	{
		if ( map_options.exportStillBlocks.packer == ALGO_RAW )
			sprintf(string, "\tdc.w 0\t; compressed size\n\n");
		else
			sprintf(string, "\tdc.w 0x%x\t; compressed size\n\n", packedSize); //force word since I doubt we'll go up to 0x10000 :)
	}
	else
		sprintf(string, "\tdc.w\t0\t ; compressed tiles size (disabled)\n");
    fwrite(string, strlen(string), 1, output);


	if (blockToExport)
	{
		sprintf(string, "\tdc.l\t%s_tiles\n", id);
	}
	else
	{
		sprintf(string, "\tdc.l\t0\t ; tiles  (disabled)\n");
	}
    fwrite(string, strlen(string), 1, output);

	sprintf(string, "\n");
    fwrite(string, strlen(string), 1, output);
	
	layercount = 0;
	for (layeridx = 0; layeridx < nbLayers; layeridx++)
	{
		if (map_options.exportLayers[layeridx].export == TRUE )
			layercount++;
	}
	sprintf(string, "\tdc.w\t%d\t ; nb layers\n",  layercount);
    fwrite(string, strlen(string), 1, output);
	
	if (layercount)
		sprintf(string, "\tdc.l\t%s_layers\n", id);
    else
		sprintf(string, "\tdc.l\t0\t ; layers  (disabled)\n");
	fwrite(string, strlen(string), 1, output);


	//pal
	if (map_options.exportStillBlocks.exportPal)
	{
		sprintf(string, "\n\t.align 1\n%s_pal:\n", id);
        fwrite(string, strlen(string), 1, output);
		image_write_pal(output, 0);
	}


	if (blockToExport) 
	{
		//tiles
		sprintf(string, "\n\t.align 1\n%s_tiles:\n", id);
        fwrite(string, strlen(string), 1, output);
		
		setbuf(stdout, NULL);

		if (verbose & (map_options.exportStillBlocks.packer != ALGO_RAW))
			printf( getCompressionResult(string, nbTiles*32, packedSize) );

		image_write_packed(output);
		image_unload();
	}


	//layers
	if (layercount)
	{
		signed short int *layer;
		
		
		sprintf(string, "\n\t.align 1\n%s_layers:\n", id);
		fwrite(string, strlen(string), 1, output);

		//write pointers
		for (layeridx = 0; layeridx < nbLayers; layeridx++)
		{
			if (map_options.exportLayers[layeridx].export == FALSE )	continue;
			
			sprintf(string, "\tdc.l\t%s_layer%d\n", id, layeridx);
			fwrite(string, strlen(string), 1, output);
		}

		
		//write data
		for (layeridx = 0; layeridx < nbLayers; layeridx++)
		{
			if (map_options.exportLayers[layeridx].export == FALSE )	continue;
			
			sprintf(string, "\n\t.align 1\n%s_layer%d:\n", id, layeridx);
			fwrite(string, strlen(string), 1, output);

			layer = mappy_getLayer(layeridx);
			if (layer == NULL)
			{
				printf("error : layer %d not found.\n", layeridx);
				continue;
			}
			
			
			for (y=0; y < header->mapheight; y++)
			{
				for (z=0; z < (header->mapblockheight/8); z++)
				{
					for (x=0; x < header->mapwidth; x++)
					{
						chunkIdx = layer[ x + header->mapwidth*y ];
						
						if (map_options.mapMode == MODE_TILE)
						{
							for (j=0; j < (header->mapblockwidth/8); j++)
							{
								sprintf(string, "\tdc.w\t0x%x\n", chunkIdx*nbTilePerBlock + j +(z*(header->mapblockwidth/8))  );
								fwrite(string, strlen(string), 1, output);
							}
						}
						else
						{
							sprintf(string, "\tdc.w\t0x%x\n", chunkIdx  );
							fwrite(string, strlen(string), 1, output);
						}
					}

					//ugly no ? ;) break ?
					if (map_options.mapMode != MODE_TILE)	z = 0xFFFF;
					
				}
			}

		}
	}
	//TODO animated block

}


static void handleStillBlockOptions(char *text)
{
	int items_read, nbChar;
	s16 nbBlock;

	trim(text, EMPTY_CHAR);

	//not option
	if (strlen(text) == 0)	return;

	items_read = sscanf(text, "%hi%n", &nbBlock, &nbChar);
	if (items_read == 1)
	{
		if (nbBlock >= header->mapnumblockstr)
		{
			printf("Error : invalid still block %d (only 0-%d are valid)\n", nbBlock, header->mapnumblockstr-1);
			return;
		}
		
		map_options.exportStillBlocks.count = nbBlock;
		text += nbChar;

		trim(text, EMPTY_CHAR);
	}
	else
	{
		//if STILLBLOCKS asked, export none by default
		map_options.exportStillBlocks.count = -1; 
	}

	if (verbose)
	{
		if (map_options.exportStillBlocks.count == -1)
		{
			printf("Doesn't export still blocks\n");
		}
		else if (map_options.exportStillBlocks.count == 0)
		{
			printf("Export %d still blocks\n", header->mapnumblockstr);
		}
		else
		{
			printf("Export %d of the %d still blocks available\n", map_options.exportStillBlocks.count-1, header->mapnumblockstr);
		}
	}

	//no more option
	if (strlen(text) == 0)	return;

	if ( strstr(text, "NO_PAL") != NULL )
	{
		map_options.exportStillBlocks.exportPal = FALSE;
	}
	if ( strstr(text, "PACKER_RLE") != NULL )
	{
			map_options.exportStillBlocks.packer = ALGO_RLE;
	}
	
	
	//TODO : handle properties
}


static void handleAnimatedBlockOptions(char *text)
{
	int items_read, nbChar;
	u16 nbAnimBlock;


	trim(text, EMPTY_CHAR);

	//no option
	if (strlen(text) == 0)	return;
	
	items_read = sscanf(text, "%hu%n", &nbAnimBlock, &nbChar);
	if (items_read == 1)
	{
		if ( (nbAnimBlock<0) || (nbAnimBlock >= header->mapnumanimstr) )
		{
			printf("Error : invalid anim block %d (only 0-%d are valid)\n", nbAnimBlock, header->mapnumanimstr-1);
			return;
		}
		map_options.exportAnimatedBlocks.count = nbAnimBlock;
		text += nbChar;

		trim(text, EMPTY_CHAR);
	}
	else
	{
		//if ANIMATEDBLOCKS asked, export all by default
		map_options.exportAnimatedBlocks.count = 0; 
	}

	if (verbose)
	{
		if (map_options.exportAnimatedBlocks.count == 0)
		{
			printf("Export all animated blocks\n");
		}
		else
		{
			printf("Export up to animated block ID %d\n", map_options.exportAnimatedBlocks.count-1);
		}
	}

	//no more option
	if (strlen(text) == 0)	return;

}

static void handleLayersOptions(char *text)
{
	//int items_read, nbChar;
	u8 layeridx;


	trim(text, EMPTY_CHAR);

	//no option to parse
	//if (strlen(text) == 0)	return;

	for (layeridx = 0; layeridx < nbLayers; layeridx++)
	{
			map_options.exportLayers[layeridx].export = TRUE;
			map_options.exportLayers[layeridx].packer = ALGO_RAW;
	}
	
	if (verbose)
	{
		printf("Export %d layers\n", nbLayers);
	}
}

static void handleLayerSoloOptions(char *text)
{
	int items_read, nbChar;
	u16 idxLayer = 0xFFFF;


	trim(text, EMPTY_CHAR);

	//no option
	if (strlen(text) == 0)	return;
	
	items_read = sscanf(text, "%hu%n", &idxLayer, &nbChar);
	if (items_read == 1)
	{
		if ( (idxLayer<0) || (idxLayer>=nbLayers) )
		{
			printf("Error : invalid layer ID %d (only 0-%d are valid)\n", idxLayer, nbLayers-1);
			return;
		}
		
		text += nbChar;

		trim(text, EMPTY_CHAR);
	}
	
	if (idxLayer == 0xFFFF)
	{
		printf("Error : invalid layer ID\n");
		return;
	}
	
	if (verbose)
	{
		printf("Export layer %d\n", idxLayer);
	}
	
	map_options.exportLayers[idxLayer].export = TRUE;		
	map_options.exportLayers[idxLayer].packer = ALGO_RAW;
	//TODO : packer
}

static void handleLine(char *text)
{
	char *ptr = malloc( strlen(text)+1 );
	char *savePtr = ptr;

    memset(ptr, 0, strlen(text)+1);
    memcpy(ptr, text, strlen(text));

    if(strncmp(ptr, "STILLBLOCKS", strlen("STILLBLOCKS")) == 0)
	{
		ptr += strlen("STILLBLOCKS");
		handleStillBlockOptions(ptr);
	}
	else if (strncmp(ptr, "ANIMATEDBLOCKS", strlen("ANIMATEDBLOCKS")) == 0)
	{
		ptr += strlen("ANIMATEDBLOCKS");
		handleAnimatedBlockOptions(ptr);
	}
	else if (strncmp(ptr, "LAYERS", strlen("LAYERS")) == 0)
	{
		ptr += strlen("LAYERS");
		handleLayersOptions(ptr);
	}
	else if (strncmp(ptr, "LAYER", strlen("LAYER")) == 0)
	{
		ptr += strlen("LAYER");
		handleLayerSoloOptions(ptr);
	}
	else
	{
		if (verbose)
		{
			printf("Unhandled line %s\n", ptr);
		}
		free(savePtr);
		return;
	}
    free(savePtr);
}


u8 parseParameters(char *text)
{
	long i, nbLines;

	struct str_parserInfo *paramParseInfo;
	paramParseInfo = parser_load(text);
	if (paramParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

    nbLines = parser_getLinesCount(paramParseInfo);
	for (i = 0; i< nbLines; i++)
	{
		handleLine((char *)parser_getLine(paramParseInfo, i));
	}
	parser_unload(paramParseInfo);

	return TRUE;
}

u8 mappy_init(char *filename)
{
	int err;
	
	
	err = mappy_load( filename );
	if (err != MER_NONE)	
	{
		err = mappy_getLastErrorID();
		if (err == MER_NOOPEN)
			printf("File not found : %s\n", filename);
		else if (err == MER_OUTOFMEM)
			printf("Not enough memory to load %s\n", filename);
		else //if (err == MER_MAPLOADERROR)
			printf("Error %d loading %s\n", err, filename);
		return FALSE;
	}
	header = mappy_getHeader();
	
	if (header->mapdepth != 8)
	{
		printf("Invalid map file : not a 8bit map\n");
		return FALSE;
	}
	
	//check chunk is 8x size
	if (header->mapblockwidth%8 != 0)
	{
		printf("Invalid map file : blocks' width isn't multiple of 8 pixels\n");
		return FALSE;
	}
	if (header->mapblockheight%8 != 0)
	{
		printf("Invalid map file : blocks' height isn't multiple of 8 pixels\n");
		return FALSE;
	}
	nbTilePerBlock = (header->mapblockwidth/8) * (header->mapblockheight/8);

	//get max number fo layers defined on map file (they could be empty but we don't really care)
	for (nbLayers = 0; nbLayers < 100; nbLayers++)
	{
			signed short int *layer = mappy_getLayer(nbLayers);
			if (layer == NULL) break;
	}

	
	return TRUE;
	
}

/////////// PUBLIC
u8 mapExecute(char *info, FILE *output)
{
	char keyword[4]; //MAP\0
	char id[50];
	char filename[256];
	char *option;
	int nbElem = 0;

	char *parameters;
	parameters = extractParameters(info);

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", filename);
	if (nbElem < 3)
	{
		printf("Wrong MAP definition\n");
		mapHelp();
		return FALSE;
	}
	info += (strlen(filename) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	
	//default value is like  MAP1.0 : 1 layer, every tiles
	
	map_options.force16   = 0;
	//tile mode
	map_options.mapMode   = MODE_TILE;

	// export all tiles, pal included but without properties
	map_options.exportStillBlocks.count= 0;
	map_options.exportStillBlocks.exportPal= TRUE;
	map_options.exportStillBlocks.packer= ALGO_RAW;
	map_options.exportStillBlocks.propertiesMask= 0;

	// don't export animated blocks
	map_options.exportAnimatedBlocks.count = -1;

	//export first layer only
	memset(&map_options.exportLayers, 0, sizeof(struct layerOptions)*MAPPY_LAYER_COUNT);
	map_options.exportLayers[0].export = TRUE;
	map_options.exportLayers[0].packer = ALGO_RAW;


	if ( strlen(info) )
	{
		option = strstr(info, "CHUNK");
		if (option != NULL)
		{
			map_options.mapMode = MODE_CHUNK;
		}
		option = strstr(info, "FORCE16");
		if (option != NULL)
		{
			map_options.force16 = 1;
		}
	}

	if (!file_isValid(filename))    return FALSE;

	if (verbose)
	{
		printf("\n%s found\n", keyword);
		printf("  id   : %s \n", id);
		printf("  file : %s \n", filename);
		printf("  mode : %s \n", (map_options.mapMode==MODE_CHUNK)?"CHUNK":"TILE");
		printf("  color: %s \n", (map_options.force16)?"reduce to 16 if needed":"use first 16");
	}

	if ( !mappy_init(filename) )
	{
		return FALSE;
	}
	
	if (parameters != NULL)
	{
		u8 layeridx;
		//if we use parameters, we're on MAP2.0, reset some default values inherited from MAP1.0
		map_options.exportLayers[0].export = FALSE;
		for (layeridx = 0; layeridx < MAPPY_LAYER_COUNT; layeridx++)
		{
			map_options.exportLayers[layeridx].export = FALSE;
			map_options.exportLayers[layeridx].packer = ALGO_RAW;
		}
			
		map_options.exportStillBlocks.count = -1;
		parseParameters(parameters);
	}

	exportMap( id, output );

	mappy_close();

	return (TRUE);
}

void mapHelp()
{
	printf("MAP extract data and tiles from a Mappy's FMP file\n");
	printf("Only 8bit tiles are supported\n");
	printf("Only the first 16 colors are imported\n");
	printf("Based on Mappy V1.3.11s by Robin Burrows\n");
	printf("Mappy is a free 2D map editor available at\n");
	printf("\t- http://www.tilemap.co.uk\n");
	printf("\t- http://www.geocities.com/SiliconValley/Vista/7336/robmpy.htm\n");
	printf("\nBasic usage:\n");
	printf("\tMAP id \"file\" [options]\n");
	printf("\t{\n");
	printf("\t\t<requested data>\n");
	printf("\t\t...\n");
	printf("\t}\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tMappy's FMP file\n");

	printf("\n  Options:\n");
	printf("  - TILE\texport tile by tile (default)\n");
	printf("  - CHUNK\texport chunk by chunk\n");
	printf("\n  - FIRST16\tuse first 16 colors (default)\n");
	printf("  - FORCE16\treduce to 16 colors if needed\n");
	
	
	printf("\n  Requested data:\n");
	//TODO : properties
	printf("  - STILLBLOCKS max_block [pal option] [packing option]\n");
	printf("    	Max number of tile/chunk to export\n");
	printf("    	    0:all\n");
	printf("    	    -1:none (default)\n");
	printf("        pal option:\n");
	printf("            NO_PAL\tdoesn't export pal\n");
	printf("        packing option:\n");
	printf("            PACKER_NONE\tdoesn't pack data (default)\n");
	printf("            PACKER_RLE\tRLE packing\n");
	//experimental
	////printf("  - ANIMATEDBLOCKS max_block [pal option] [packing option]\n");
	////printf("    	Max number of tile/chunk to export (0:all)\n");
	printf("  - LAYERS\n");
	printf("    	export all valids layers\n");
	//TODO : pack ?
	printf("  - LAYER index\n");
	printf("    	export layer <index> (0-%d)\n", MAPPY_LAYER_COUNT-1);

	

	printf("\nExample:\n");

	printf("MAP level1 \"sonic_level1.map\" CHUNK FORCE16\n");
	printf("{\n");
	printf("\tSTILLBLOCKS 0 NO_PAL PACKER_RLE\n");
	//experimental
	////printf("\tANIMATEDBLOCKS 0 NO_PAL\n");
	printf("\tLAYER 0\n");
	printf("\tLAYER 1\n");
	printf("}\n");
	
}
