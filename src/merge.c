#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/packer.h"
#include "utils/parser.h"


struct options
{
	u8 exportHorizontal;
} merge_options;

static FIBITMAP *dib;

FIBITMAP *image_open(char *filename)
{
	FREE_IMAGE_FORMAT fif;
	FIBITMAP *input;

	fif = FreeImage_GetFileType(filename, 0);
	if(fif == FIF_UNKNOWN) {
		// no signature ?
		// try to guess the file format from the file extension
		fif = FreeImage_GetFIFFromFilename(filename);
	}

	if(fif == FIF_UNKNOWN)
	{
		printf("Unknown file format\n");
		return NULL;
	}

	//if png, FreeImage_Load(fif, filename, PNG_IGNOREGAMMA);

	input = FreeImage_Load(fif, filename, 0);
	if (input == NULL)
	{
		printf("Can't open file %s\n", filename);
		return NULL;
	}

	if ( FreeImage_GetColorsUsed(input) == 0 )
	{
		printf("Hi colors bitmap not supported\n");
		return NULL;
	}

	return input;
}


static u8 handleLine( char *text )
{
	FIBITMAP *dibInput;
	RGBQUAD rgb;
	int tmp, expandSize;

	char *ptr = malloc( strlen(text)+1 );
	char *savePtr = ptr;

    memset(ptr, 0, strlen(text)+1);
    memcpy(ptr, text, strlen(text));


	if ( strncmp(ptr, "FILE" , strlen("FILE")) == 0 )
	{
		ptr += strlen("FILE");
	}
	else
	{
        free(savePtr);
		return FALSE;
	}

	//ptr is now filename
	trim(ptr, EMPTY_CHAR);

	//remove quote
	trim(ptr, "\"");

	dibInput = image_open(ptr);
	if (dibInput == NULL)	return FALSE;

	if (dib == NULL)
	{
		dib = FreeImage_Clone(dibInput);
		FreeImage_Unload(dibInput);
		free(savePtr);
		return TRUE;
	}


	rgb.rgbReserved = 0;

	if (merge_options.exportHorizontal)
	{
		tmp  = FreeImage_GetWidth(dib);
		expandSize =  FreeImage_GetHeight(dibInput) - FreeImage_GetHeight(dib);
		if (expandSize < 0)	expandSize = 0;

		dib = FreeImage_EnlargeCanvas(dib, 0, 0, FreeImage_GetWidth(dibInput), expandSize, &rgb, FI_COLOR_ALPHA_IS_INDEX);
		free(savePtr);

		return FreeImage_Paste(dib, dibInput, tmp, 0, 256);
	}

	tmp  = FreeImage_GetHeight(dib);
	expandSize = FreeImage_GetWidth(dibInput) - FreeImage_GetWidth(dib);
	if (expandSize < 0)	expandSize = 0;
	dib = FreeImage_EnlargeCanvas(dib, 0, 0, expandSize, FreeImage_GetHeight(dibInput), &rgb, FI_COLOR_ALPHA_IS_INDEX);
	free(savePtr);

	return FreeImage_Paste(dib, dibInput, 0, tmp, 256);
}


static u8 processFile( char *filelist )
{
	long i;
	long nbLines;

	struct str_parserInfo *filesParseInfo;

	filesParseInfo = parser_load( filelist );
	if (filesParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

	if (dib != NULL)
	{
		FreeImage_Unload(dib);
		dib = NULL;
	}

    nbLines = parser_getLinesCount(filesParseInfo);
	for(i=0; i< nbLines; i++)
	{
		handleLine( (char *) parser_getLine(filesParseInfo, i)  );
	}

	parser_unload(filesParseInfo);

	return TRUE;
}
////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 mergeExecute(char *info, FILE *output)
{
	char keyword[6]; //MERGE\0
	char file[MAX_PATH];
	char *option;
	char *filelist;
	int nbElem = 0;
	FREE_IMAGE_FORMAT fif;

	filelist = extractParameters(info);

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	info += (strlen(file) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);


	if ( nbElem < 2 )
	{
		printf("Wrong MERGE definition\n");
		mergeHelp();
		return FALSE;
	}

	fif = FreeImage_GetFIFFromFilename(file);
	if(fif == FIF_UNKNOWN)
	{
		printf("Unknow file format %s\n", file);
		return FALSE;
	}
	//init default
	merge_options.exportHorizontal = TRUE;


	if ( strlen(info) )
	{
		option = strstr(info, "VERTICAL");
		if (option != NULL)
		{
			merge_options.exportHorizontal = FALSE;
		}
	}

	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //MERGE
		printf("  file : %s \n", file);
		printf("  merge: %s \n", (merge_options.exportHorizontal?"Horizontal":"Vertical"));
	}

	processFile(filelist);

	if (dib != NULL)
	{
		FreeImage_Save(fif, dib, file, 0);
		FreeImage_Unload(dib);
		dib = NULL;
	}

	 return TRUE;
}

void mergeHelp()
{
	printf("MERGE file\n");
	printf("  file\t\tpath of result merge file\n");

	printf("  Options:\n");
	printf("  HORIZONTAL\tmerge horizontaly (default)\n");
	printf("  VERTICAL\tmerge verticaly\n\n");
}
