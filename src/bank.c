#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/parser.h"
#include "utils/file_utils.h"


////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 bankExecute(char *info, FILE *output)
{
	char keyword[5]; //BANK\0
	char id[50];
	int nbElem = 0;
	char string[MAX_PATH];

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	if (nbElem < 2)
	{
		printf("Wrong BANK definition\n");
		bankHelp();
		return FALSE;
	}

	if (verbose)
	{
		printf("\n%s found\n", keyword);
		printf("  id   : %s \n", id);
	}

	sprintf(string, ";;;;;;;;;; BANK \n\t.section %s\n\n", id);
	fwrite(string, strlen(string), 1, output);

	return TRUE;
}

void bankHelp()
{
	printf("BANK primilary bank support\n");
	printf("for now, let's you include a \".section\" directive anywhere in your resource\n");
	printf("\nBasic usage:\n");
	printf("\tBANK id \n");

	printf("\nwhere\n");

	printf("  id\t\tsection name, defined in md.ld\n");
	
	printf("\nExample:\n");
	printf("BANK .bank1");
}
