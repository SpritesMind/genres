#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/image_utils.h"
#include "utils/packer.h"
#include "utils/parser.h"


struct options
{
	int	 frameWidth, frameHeight;
	u8 exportPal;
	u8 exportPixels;
	u8 exportAnimations;
	char alphaCode;
	u8 keepEmpty;
	char collideMode;	//0: none
						//1: auto
						//2: manual
} spritesheet_options;


struct animInfo
{
	u8 frameRate;
	u8 frameCount;
	char *frames; //joined array !
};

typedef struct {
  struct animInfo **array;
  size_t used;
  size_t size;
}AnimArray;

AnimArray *anims;


void initAnimArray(AnimArray *a, size_t initialSize)
{
  a->array = (struct animInfo **)malloc(initialSize * sizeof(struct animInfo *));
  a->used = 0;
  a->size = initialSize;
}

void insertAnimArray(AnimArray *a, struct animInfo *info)
{
  if (a->used == a->size) {
    a->size *= 2;
    a->array = (struct animInfo **)realloc(a->array, a->size * sizeof(struct animInfo *));
  }
  a->array[a->used++] = info;
}

void freeAnimArray(AnimArray *a)
{
  size_t i;
  for (i=0; i < a->used; i++)
	  free( a->array[i] );

  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}


static struct animInfo *handleLine( char *text )
{
	int items_read, i;
	int nbChar;
	int frameRate;
	struct animInfo *info;

	char *ptr = malloc( strlen(text)+1 );
	char *savePtr = ptr;

    memset(ptr, 0, strlen(text)+1);
    memcpy(ptr, text, strlen(text));

	if ( strncmp(ptr, "ANIM" , strlen("ANIM")) == 0 )
	{
		ptr += strlen("ANIM");
	}
	else
	{
        free(savePtr);
		return NULL;
	}

	trim(ptr, EMPTY_CHAR);

	info = (struct animInfo *) malloc( sizeof(struct animInfo) );
	items_read = sscanf(ptr, "%d%n", &frameRate, &nbChar);
	if (items_read != 1)
	{
        free(savePtr);
		return NULL;
    }

	info->frameRate = frameRate;

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	if (strlen(ptr) == 0)
	{
        free(savePtr);
        free(info);
		return NULL; //no data after ANIM <framerate>
    }

	info->frames = (char *) malloc( strlen(ptr) + 1);
	memset(info->frames, 0, strlen(ptr) + 1);

	strcpy(info->frames, ptr);

	info->frameCount = 1;
	for (i=0; ptr[i]; i++)
	{
		if ( ptr[i]==',' )
			info->frameCount++;
	}

    free(savePtr);

	return info;
}


u8 parseAnim( char *text )
{
	long i, nbLines;

	struct str_parserInfo *animsParseInfo;



	animsParseInfo = parser_load( text );
	if (animsParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

    nbLines = parser_getLinesCount(animsParseInfo);
	for(i=0; i< nbLines; i++)
	{
		struct animInfo *info = handleLine( (char *) parser_getLine(animsParseInfo, i)  );
		if (info != NULL)
			insertAnimArray(anims, info);
	}
	parser_unload(animsParseInfo);

	return TRUE;
}

char getBestSpriteSize( int pixels )
{
	char tmp;
	char best, bestSize;

	tmp = pixels/8;
	if (pixels%8)	tmp++;
	bestSize = 8;
	best = tmp;

	tmp = pixels/16;
	if (pixels%16)	tmp++;
	if ( best > tmp )
	{
		bestSize = 16;
		best = tmp;
	}

	tmp = pixels/24;
	if (pixels%24)	tmp++;
	if ( best > tmp )
	{
		bestSize = 24;
		best = tmp;
	}

	tmp = pixels/32;
	if (pixels%32)	tmp++;
	if ( best > tmp )
	{
		bestSize = 32;
		best = tmp;
	}

	return bestSize;
}

static void spritesheet_convert( FILE *output, char *id, int frameWidth, int frameHeight )
{
	int i, j,k;
	int	frameTiles;
	int paddedX, paddedY;
    int frameSpriteH, frameSpriteV;
	int spriteWidth, spriteHeight;
	int size;
	char    string[256];


	
	image_pack_tiles(ALGO_RAW, 0);
	
	paddedX = nbTilesX*8;
	paddedY = nbTilesY*8;
	
	if (verbose)
		printf("  Padded size %d,%d\n", paddedX, paddedY);


	spriteWidth = getBestSpriteSize(frameWidth);
	spriteHeight = getBestSpriteSize(frameHeight);


	frameSpriteH = frameWidth / spriteWidth;
	if (frameWidth%spriteWidth)		frameSpriteH++;

	frameSpriteV = frameHeight / spriteHeight;
	if (frameHeight%spriteHeight)	frameSpriteV++;

	frameTiles = ( (frameSpriteH * spriteWidth)/8 ) * ( (frameSpriteV*spriteHeight)/8 );

	size = (spriteWidth/8)-1;
	size <<= 2;
	size |= (spriteHeight/8)-1;
	// not SGDK compatible
	//size <<= 8;

	//header
	sprintf(string, ";;;;;;;;;; SPRITESHEET \n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);

	if (spritesheet_options.exportAnimations)
	{
		sprintf(string, "\tdc.w %d\t; nb Animation\n", anims->used);
		fwrite(string, strlen(string), 1, output);
	}
	else
	{
		sprintf(string, "\tdc.w 0\t; NO_ANIMATIONS\n");
		fwrite(string, strlen(string), 1, output);
	}


	if (frameSpriteH == 256)
		sprintf(string, "\tdc.b 0\t; nb sprite horizontal (256px)\n");
	else
		sprintf(string, "\tdc.b %d\t; nb sprite horizontal\n", frameSpriteH);
	fwrite(string, strlen(string), 1, output);

	if (frameSpriteV == 256)
			sprintf(string, "\tdc.b 0\t; nb sprite vertical (256px)\n");
		else
			sprintf(string, "\tdc.b %d\t; nb sprite vertical\n", frameSpriteV);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b %d\t; width in pixel of a frame\n", frameWidth);
	fwrite(string, strlen(string), 1, output);
	sprintf(string, "\tdc.b %d\t; height\n", frameHeight);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.w %d\t; nb tiles needed for a frame\n", frameTiles);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0x%.2x\t; SGDK compliant unique sprite size\n", size);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0\t; padding\n\n");
	fwrite(string, strlen(string), 1, output);


	if (spritesheet_options.exportPal)
		sprintf(string, "\tdc.l %s_pal\t; pal data adress\n", id);
	else
		sprintf(string, "\tdc.l 0\t; NO_PAL\n");
	fwrite(string, strlen(string), 1, output);

	if (spritesheet_options.exportAnimations)
	{
		sprintf(string, "\tdc.l %s_animations\t; pointer to anim infos\n", id);
		fwrite(string, strlen(string), 1, output);
	}
	else
	{
		sprintf(string, "\tdc.l 0\t; NO_ANIMATIONS\n");
		fwrite(string, strlen(string), 1, output);
	}

	if (spritesheet_options.exportPixels)
		sprintf(string, "\tdc.l %s_frames\t;\n", id);
	else
		sprintf(string, "\tdc.l 0\t; NO_PIXELS\n");
	fwrite(string, strlen(string), 1, output);

	if (spritesheet_options.collideMode == 0)
		sprintf(string, "\tdc.l 0\t; NO_COLLIDE\n");
	else
		sprintf(string, "\tdc.l %s_collides\t;\n", id);
	fwrite(string, strlen(string), 1, output);


	//pal
	if (spritesheet_options.exportPal)
	{
		sprintf(string, "\n\t.align 2\n%s_pal:\n", id);
		fwrite(string, strlen(string), 1, output);

		image_write_pal(output, 0);
	}

	//frames (and collidebox?)
	if (spritesheet_options.exportPixels || (spritesheet_options.collideMode != 0) )
	{

		int	nbFrames;
		int	x, y;
		x = y = 0;

		//sprites' tiles
		if (spritesheet_options.exportPixels)
		{
			sprintf(string, "\n\t.align 2\n%s_frames:\n", id);
			fwrite(string, strlen(string), 1, output);

			nbFrames = (int) ((nbTilesX*8)/frameWidth)*((nbTilesY*8)/frameHeight);

			for (j=0; j< nbFrames; j++)
			{
				
				sprintf(string, "\tdc.l\t%s_frame%.2d\n", id, j);
				fwrite(string, strlen(string), 1, output);
			}
		}

		//sprites' collide
		if (spritesheet_options.collideMode != 0)
		{
			sprintf(string, "\n\t.align 2\n%s_collides:\n", id);
			fwrite(string, strlen(string), 1, output);

			nbFrames = (int) ((nbTilesX*8)/frameWidth)*((nbTilesY*8)/frameHeight);
			for (j=0; j< nbFrames; j++)
			{
				sprintf(string, "\tdc.l\t%s_collide%.2d\n", id, j);
				fwrite(string, strlen(string), 1, output);
			}
		}

		sprintf(string, "\n\t.align 2\n");
		fwrite(string, strlen(string), 1, output);

		i = 0;
		for (y=0; y < paddedY; y+= frameHeight)
		{
			for (x=0; x < paddedX; x+= frameWidth)
			{
				int	curSpriteX, curSpriteY;
				FIBITMAP *clone= image_clone( x, y, frameWidth, frameHeight);

				if (spritesheet_options.exportPixels)
				{
					image_make_tiles(clone, ALGO_RAW, 0);
					sprintf(string, "%s_frame%.2d:\n", id, i);
					fwrite(string, strlen(string), 1, output);
					/*
					printf("%d %d\n", frameSpriteH, frameSpriteV);
					printf("%d %d\n", spriteWidth, spriteHeight);
					*/

					for(curSpriteY=0; curSpriteY< frameSpriteV; curSpriteY++)
					{
						for(curSpriteX=0; curSpriteX< frameSpriteH; curSpriteX++)
						{
							for(j=0;j<(spriteWidth/8);j++)
							{

								for( k=0; k<(spriteHeight/8); k++)
								{
									int curTile = curSpriteY * (spriteHeight/8); //nb row
									curTile *= ( frameSpriteH * (spriteWidth/8));  //* nbCol

									curTile += ( curSpriteX * (spriteWidth/8));
									curTile += ( k * frameSpriteH * (spriteWidth/8));
									curTile += j;


									//printf("Tile %d\n", curTile);
									image_write_tile(output, curTile);

									sprintf(string, "\n");
									fwrite(string, strlen(string), 1, output);
								}
							}
						}
					}
				}

				if (spritesheet_options.collideMode != 0)
				{
					sprintf(string, "%s_collide%.2d:\n", id, i);
					fwrite(string, strlen(string), 1, output);

					image_write_box(clone, output, FALSE);
				}

				i++;
			}
		}
	}

	//animation
	if (spritesheet_options.exportAnimations)
	{
		sprintf(string, "\n\t.align 2\n%s_animations:\n", id);
		fwrite(string, strlen(string), 1, output);

		for (j=0; j < anims->used; j++)
		{
			sprintf(string, "\tdc.l\t%s_anim%.2d\n", id, j);
			fwrite(string, strlen(string), 1, output);;
		}

		for (j=0; j < anims->used; j++)
		{
			struct animInfo *info = anims->array[j];
			sprintf(string, "\n\t.align 2\n%s_anim%.2d:\n", id, j);
			fwrite(string, strlen(string), 1, output);

			sprintf(string, "\tdc.b %d ; frame rate\n", info->frameRate);
			fwrite(string, strlen(string), 1, output);
			sprintf(string, "\tdc.b %d ; frame count\n", info->frameCount);
			fwrite(string, strlen(string), 1, output);
			sprintf(string, "\tdc.b %s\n", info->frames);
			fwrite(string, strlen(string), 1, output);
		}
	}
}



static u8 processFile( FILE *output, char *id, char *filename )
{
	if (!image_load_tiles( filename, spritesheet_options.alphaCode))
	{
		printf("File not found\n");
		freeAnimArray(anims);
		return FALSE;
	}

	spritesheet_convert(output, id, spritesheet_options.frameWidth, spritesheet_options.frameHeight);

	image_unload();

	freeAnimArray(anims);

	return TRUE;
}



////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 spritesheetExecute(char *info, FILE *output)
{
	char keyword[12]; //SPRITESHEET\0
	char id[50];
	char buffer[10];
	char file[MAX_PATH];
	char *option;
	char *animations;
	int	frameWidth, frameHeight;
	int nbElem = 0;


	animations = extractParameters(info);

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	info += (strlen(file) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%d", &frameWidth);
	info += sprintf ( buffer, "%d", frameWidth);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%d", &frameHeight);
	info += sprintf ( buffer, "%d", frameHeight);
	trim(info, EMPTY_CHAR);

	if ( nbElem < 5 )
	{
		printf("Wrong SPRITESHEET definition\n");
		spritesheetHelp();
		return FALSE;
	}

	if ( (frameWidth > 256) || (frameHeight > 256) )
	{
		printf("Error : max size of a frame for a sprite is 256x256\n");
		return FALSE;
	}

	anims = (AnimArray *) malloc( sizeof( AnimArray ) );
	initAnimArray(anims, 1);
	if (animations != NULL)
		parseAnim( animations );


	//init default
	spritesheet_options.frameWidth = frameWidth;
	spritesheet_options.frameHeight = frameHeight;
	spritesheet_options.exportPal = TRUE;
	spritesheet_options.exportPixels = TRUE;
	spritesheet_options.exportAnimations = TRUE;
	spritesheet_options.keepEmpty = TRUE;
	spritesheet_options.alphaCode = ALPHA_TOPLEFT;
	spritesheet_options.collideMode = 1;

	if ( strlen(info) )
	{
		option = strstr(info, "NO_PAL");
		if (option != NULL)
		{
			spritesheet_options.exportPal = FALSE;
		}

		option = strstr(info, "NO_PIXEL");
		if (option != NULL)
		{
			spritesheet_options.exportPixels = FALSE;
		}

		option = strstr(info, "NO_ANIMATION");
		if (option != NULL)
		{
			spritesheet_options.exportAnimations = FALSE;
		}

		option = strstr(info, "ALPHA_NONE");
		if (option != NULL)
		{
			spritesheet_options.alphaCode = ALPHA_NONE;
		}

		option = strstr(info, "ALPHA_BOTTOMRIGHT");
		if (option != NULL)
		{
			spritesheet_options.alphaCode = ALPHA_BOTTOMRIGHT;
		}

		option = strstr(info, "SKIP_EMPTY");
		if (option != NULL)
		{
			spritesheet_options.keepEmpty = FALSE;
		}

		option = strstr(info, "NO_COLLIDE");
		if (option != NULL)
		{
			spritesheet_options.collideMode = 0;
		}
		else
		{
			option = strstr(info, "MANUAL_COLLIDE");
			if (option != NULL)
			{
				spritesheet_options.collideMode = 2;
			}
		}
	}


	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //SPRITE
		printf("  id   : %s \n", id);
		printf("  file : %s \n", file);
		printf("  width: %d \n", spritesheet_options.frameWidth);
		printf("  height:%d \n", spritesheet_options.frameHeight);
		printf("  pal  : %d \n", spritesheet_options.exportPal);
		printf("  pixel: %d \n", spritesheet_options.exportPixels);
		//printf("  empty: %d \n", spritesheet_options.keepEmpty);
		printf("  alpha: %s \n", ((spritesheet_options.alphaCode == ALPHA_NONE) ? "NONE" : ((spritesheet_options.alphaCode == ALPHA_TOPLEFT) ? "TopLeft" : "BottomRight")));
	}

	return processFile(output, id, file);
}

void spritesheetHelp()
{
	printf("SPRITESHEET create ready to use animated sprite from any 16 or 256 colors.\n");
	printf("Define animation and collision right from a resource file\n");
	printf("A frame use basic (32pixels max) or multi sprites (256pixels max)\n");
	printf("Only the first 16 colors are imported\n");
	printf("\nBasic usage:\n");
	printf("\tSPRITESHEET id \"file\" width height [options]\n");
	printf("\t{\n");
	printf("\t\tANIM <frameRate> <frameId>,<frameId>,...\n");
	printf("\t\t...\n");
	printf("\t}\n");


	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tthe bitmap to convert\n");
	printf("  width\t\twidth of each frame\n");
	printf("  height\theight of each frame\n");

	printf("\n  Options:\n");
	printf("  - NO_PAL\t\tdoesn't export pal\n");
	printf("  - NO_PIXELS\t\tdoesn't export tiles, only bitmap info\n");
	printf("  - NO_ANIMATIONS\tdoesn't export animations, if any\n");
	printf("\n  Collide options:\n");
	printf("  - AUTO_COLLIDE\tdetect collide from non-transparent pixels (default)\n");
	//printf("  MANUAL_COLLIDE\tcollide box defined as a sub data\n");
	printf("  - NO_COLLIDE\t\tdoesn't store collide infos\n");
	//printf("\n  Optimize options:\n");
	//printf("  KEEP_EMPTY\tkeep zero'ed sprite (default)\n");
	//printf("  SKIP_EMPTY\tremove zero'ed sprite\n");
	printf("\n  Alpha options:\n");
	printf("  - ALPHA_TOPLEFT\tuse pixel at top left as transparent color (default)\n");
	printf("  - ALPHA_BOTTOMRIGHT\tuse pixel at top left as transparent color\n");
	printf("  - ALPHA_NONE\t\tkeep color 0 as transparent color\n\n");

	printf("\n  Animations:\n");
	printf("  - ANIM <frameRate> <frameList>\n");
	printf("\tframeRate is vint count before next frame\n");
	printf("\tframeList is a comma separated frame list\n");

	printf("\nExample (using standard sprite size):\n");

	printf("SPRITESHEET bjack_sheet \"data/bombjack.bmp\" 16 16 NO_COLLIDE\n");
	printf("{\n");
	printf("\tANIM\t10 27,28,29,30\n");
	printf("\tANIM\t30 24,25,25,25,25,35,36,36,36,36,26\n");
	printf("\tANIM\t10 12,13,14,15\n");
	printf("\tANIM\t10 16,17,18,19\n");
	printf("\tANIM\t10 20,21,22,23\n");
	printf("}");

}

