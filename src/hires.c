#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"

#include "utils/image_utils.h"
#include "utils/parser.h"
#include "utils/packer.h"


struct options
{
	u8 exportPal;
	u8 exportPixels;
	char packer;
	char alphaCode;
} hires_options;

void hires_convert( FILE *output, char *id  )
{
	char	string[256];

	//header
	sprintf(string, ";;;;;;;;;; HIRES \n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);
	//const unsigned short width;
	//const unsigned short height;
	//const unsigned short *pal1;
	//const unsigned short *pal2;
	//const unsigned short *plane1;
	//const unsigned short *plane2;
	//const unsigned short packedSize1;
	//const unsigned short packedSize2;

	//color 16 is color 0, and color 31 is unused so ...
	image_switch_color(16, 31);


	//pack pal 0 for reference
	image_pack_tiles(hires_options.packer, 0);

	sprintf(string, "\tdc.w %d\t; Width\n", nbTilesX);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.w %d\t; Height\n", nbTilesY);
	fwrite(string, strlen(string), 1, output);
		
	if (hires_options.exportPal)
		sprintf(string, "\tdc.l %s_pal1, %s_pal2\t; pal data adress\n", id, id);
	else
		sprintf(string, "\tdc.l 0,0\t; NO_PAL (disabled)\n");
	fwrite(string, strlen(string), 1, output);
	
	
	if (hires_options.exportPixels)
		sprintf(string, "\tdc.l %s_plane1, %s_plane2\t; tiles data adress\n", id, id);
	else
		sprintf(string, "\tdc.l 0,0\t; NO_PIXELS (disabled)\n");
	fwrite(string, strlen(string), 1, output);


	

	
	if (hires_options.exportPixels == FALSE)
	{
		sprintf(string, "\tdc.w 0,0\t; compressed size (disabled)\n\n");
	}
	else if (hires_options.packer == ALGO_RAW) 
	{
		sprintf(string, "\tdc.w 0,0\t; PACKER_NONE\n\n");
	}
	else
	{
		image_pack_tiles(hires_options.packer, 0);
		sprintf(string, "\tdc.w 0x%x\t; compressed size\n", packedSize); //force word since I doubt we'll go up to 0x10000 :)
		fwrite(string, strlen(string), 1, output);
		
		image_pack_tiles(hires_options.packer, 1);
		sprintf(string, "\tdc.w 0x%x\n\n", packedSize); //force word since I doubt we'll go up to 0x10000 :)
	}
	fwrite(string, strlen(string), 1, output);

	


	//pal
	if (hires_options.exportPal)
	{
		sprintf(string, "\n\t.align 1\n%s_pal1:\n", id);
		fwrite(string, strlen(string), 1, output);
		image_write_pal(output, 0);
		
		sprintf(string, "\n\t.align 1\n%s_pal2:\n", id);
		fwrite(string, strlen(string), 1, output);
		image_write_pal(output, 1);
	}

	//tiles 1
	if (hires_options.exportPixels)
	{
		sprintf(string, "\n\t.align 1\n%s_plane1:\n", id);
		fwrite(string, strlen(string), 1, output);

		setbuf(stdout, NULL);

		
		image_pack_tiles(hires_options.packer, 0);
		if (verbose & (hires_options.packer != ALGO_RAW))
			printf( getCompressionResult(string, nbTiles*32, packedSize) );

		image_write_packed(output);
	}

	//tiles 2
	if (hires_options.exportPixels)
	{
		sprintf(string, "\n\t.align 1\n%s_plane2:\n", id);
		fwrite(string, strlen(string), 1, output);

		setbuf(stdout, NULL);

		
		image_pack_tiles(hires_options.packer, 1);
		if (verbose & (hires_options.packer != ALGO_RAW))
			printf( getCompressionResult(string, nbTiles*32, packedSize) );

		image_write_packed(output);
	}

	sprintf(string, "\n");
	fwrite(string, strlen(string), 1, output);
}


////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 hiresExecute(char *info, FILE *output)
{
	char keyword[6]; //HIRES\0
	char id[50];
	char file[MAX_PATH];
	char *option;
	int nbElem = 0;

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	if (nbElem < 3)
	{
		printf("Wrong HIRES definition\n");
		bitmapHelp();
		return FALSE;
	}
	info += (strlen(file) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	//init default
	hires_options.exportPal = TRUE;
	hires_options.exportPixels = TRUE;
	hires_options.packer = ALGO_RAW;
	hires_options.alphaCode = ALPHA_TOPLEFT;

	if ( strlen(info) )
	{
		option = strstr(info, "NO_PAL");
		if (option != NULL)
		{
			hires_options.exportPal = FALSE;
		}

		option = strstr(info, "NO_PIXEL");
		if (option != NULL)
		{
			hires_options.exportPixels = FALSE;
		}

		option = strstr(info, "PACKER_RLE");
		if (option != NULL)
		{
			hires_options.packer = ALGO_RLE;
		}

		option = strstr(info, "ALPHA_NONE");
		if (option != NULL)
		{
			hires_options.alphaCode = ALPHA_NONE;
		}

		option = strstr(info, "ALPHA_BOTTOMRIGHT");
		if (option != NULL)
		{
			hires_options.alphaCode = ALPHA_BOTTOMRIGHT;
		}
	}

	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //HIRES
		printf("  id   : %s \n", id);
		printf("  file : %s \n", file);
		printf("  comp : %s \n", getCompressionName(hires_options.packer) );
		printf("  pal  : %d \n", hires_options.exportPal);
		printf("  pixel: %d \n", hires_options.exportPixels);
		printf("  alpha: %s \n", ((hires_options.alphaCode == ALPHA_NONE) ? "NONE" : ((hires_options.alphaCode == ALPHA_TOPLEFT) ? "TopLeft" : "BottomRight")));
	}


	if (!image_load_tiles(file, hires_options.alphaCode))
	{
		printf("File not found\n");
		return FALSE;
	}

	hires_convert(output, id);

	image_unload();

	return TRUE;
}

void hiresHelp()
{
	printf("HIRES lets you import a 32 or 256 colors bitmap\n");
	printf("The first 16 colors are imported as plane 1\n");
	printf("The 16 following colors are imported as plane 2\n");
//	printf("See sample for usage.\n");
	printf("\nBasic usage:\n");
	printf("\tHIRES id \"file\" [options]\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tthe bitmap to convert\n");

	printf("\n  Options:\n");
	printf("  - NO_PAL\tdoesn't export the 2 pals\n");
	printf("  - NO_PIXELS\tdoesn't export tiles, only bitmap info\n");
	printf("\n  Packing options:\n");
	printf("  - PACKER_NONE\tdoesn't pack data (default)\n");
	printf("  - PACKER_RLE\tRLE packing\n");
	printf("\n  Alpha options:\n");
	printf("  - ALPHA_TOPLEFT\tuse pixel at top left as transparent color (default)\n");
	printf("  - ALPHA_BOTTOMRIGHT\tuse pixel at top left as transparent color\n");
	printf("  - ALPHA_NONE\t\tkeep color 0 as transparent color\n\n");

	printf("\nExample:\n");
	printf("HIRES main_logo \"resource\\logo.bmp\" PACKER_NONE ALPHA_NONE");
}