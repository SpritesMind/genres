#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/file_utils.h"
#include "utils/echo_utils.h"
#include "utils/midi_utils.h"
#include "utils/parser.h"

struct options
{

} echo_sfx_options;

struct duo{
	const char *name;
	int value;
};
static const struct duo mappingNames[NUM_CHAN]={
		{ "FM1"   , CHAN_FM1 },
		{ "FM2"   , CHAN_FM2 },
		{ "FM3"   , CHAN_FM3 },
		{ "FM4"   , CHAN_FM4 },
		{ "FM5"   , CHAN_FM5 },
		{ "FM6"   , CHAN_FM6 },
		{ "PSG1"  , CHAN_PSG1 },
		{ "PSG2"  , CHAN_PSG2 },
		{ "PSG3"  , CHAN_PSG3 },
		{ "PSG4"  , CHAN_PSG4 },
		{ "PCM"   , CHAN_PCM }
};


static int channelID;
static int instrumentID;
static int instrumentType = -1;
static int volume = 0xFF ; //TODO
static int note = 0x48; //TODO total random value
static int duration = -1;

static void handlePCM(char *text)
{
	//
	int items_read;
	int nbChar;
	char *ptr = text;

	instrumentType =  INSTR_PCM;

	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &instrumentID, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read PCM voice id from %s\n", ptr);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &duration, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read duration from %s\n", text);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);
}
static void handleFM(char *text)
{
	int items_read;
	int nbChar;
	char channelName[7];
	char *ptr = text;

	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &instrumentID, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read instrument ID from %s\n", text);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &note, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read note from %s\n", text);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &duration, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read duration from %s\n", text);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);


	//handle chanel code or index(?)
	channelID = CHAN_FM1;
	items_read = sscanf(ptr, "%s%n", channelName, &nbChar);
	if ( items_read == 1 )
	{
		int i;
		for (i = 0; i < NUM_CHAN-1; i++)
		{
			if ( strcmp( channelName, mappingNames[i].name ) == 0)
			{
				channelID = mappingNames[i].value;
				break;
			}
		}

		if ( (channelID == CHAN_NONE) || (channelID > CHAN_FM6))
		{
			printf("Invalid Echo channel name : %s\n", channelName);
			printf("Valid names :\n");
			for (i = 0; i < CHAN_PSG1; i++)
			{
				printf("%s, ", mappingNames[i].name);
			}
			printf("\n");
			return;
		}
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);
}
static void handlePSG(char *text)
{
	int items_read;
	int nbChar;
	char channelName[7];
	char *ptr = text;

	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &instrumentID, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read instrument ID from %s\n", text);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &note, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read note from %s\n", text);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &duration, &nbChar);
	if (items_read != 1)
	{
		printf("Unable to read duration from %s\n", text);
		return;
	}

	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);


	//handle chanel code or index(?)
	channelID = CHAN_PSG1;
	items_read = sscanf(ptr, "%s%n", channelName, &nbChar);
	if ( items_read == 1 )
	{
		int i;
		for (i = 0; i < NUM_CHAN; i++)
		{
			if ( strcmp( channelName, mappingNames[i].name ) == 0)
			{
				channelID = mappingNames[i].value;
				break;
			}
		}

		if ( (channelID == CHAN_NONE) || (channelID < CHAN_PSG1) || (channelID > CHAN_PSG4) )
		{
			printf("Invalid Echo channel name : %s\n", channelName);
			printf("Valid names :\n");
			for (i = CHAN_PSG1; i < CHAN_PSG4EX; i++)
			{
				printf("%s, ", mappingNames[i].name);
			}
			printf("\n");
			return;
		}
	}


	ptr += nbChar;
	trim(ptr, EMPTY_CHAR);
}

static void handleLine(char *text)
{
	char *ptr = malloc( strlen(text)+1 );
	char *savePtr = ptr;

    memset(ptr, 0, strlen(text)+1);
    memcpy(ptr, text, strlen(text));


	if ( strncmp(ptr, "PCM" , strlen("PCM")) == 0 )
	{
		ptr += strlen("PCM");
		instrumentType = INSTR_PCM;
		handlePCM(ptr);
	}
	else if(strncmp(ptr, "FM", strlen("FM")) == 0)
	{
		ptr += strlen("FM");
		instrumentType = INSTR_FM;
		handleFM(ptr);
	}
	else if (strncmp(ptr, "PSG", strlen("PSG")) == 0)
	{
		ptr += strlen("PSG");
		instrumentType = INSTR_PSG;
		handlePSG(ptr);
	}


	free(savePtr);
	return;
}


u8 parseBankInfo(char *text)
{
	long i, nbLines;
	struct str_parserInfo *mappingParseInfo;

	instrumentType = -1;
	instrumentID = -1;
	channelID = CHAN_NONE;
	duration = -1;

	mappingParseInfo = parser_load(text);
	if (mappingParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

    nbLines = parser_getLinesCount(mappingParseInfo);
	for (i = 0; i< nbLines; i++)
	{
		handleLine((char *)parser_getLine(mappingParseInfo, i));
	}

	parser_unload(mappingParseInfo);

	return TRUE;
}



static u8 processFile(FILE *output, char *id, char *filename)
{
	char outputFilename[MAX_PATH];
	char	string[MAX_PATH];
	char	*ext;

	ext = file_getExtension(_strlwr(filename));

	if (strncmp(ext, "esf", strlen("esf")) == 0)
	{
		strcpy(outputFilename, filename);
	}
	else
	{
		printf("Format %s not supported.\n", ext);
		return FALSE;
	}

	sprintf(string, ";;;;;;;;;; ECHO_SFX\n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tINCBIN \"%s\"\n", outputFilename);
	fwrite(string, strlen(string), 1, output);

	return TRUE;
}

static u8 generateSFX(FILE *output, char *id)
{
	char	string[MAX_PATH];

	if (instrumentType == -1)
	{
		printf("Missing instrument type");
		return FALSE;
	}

	if (instrumentID == -1)
	{
		printf("Missing instrument ID");
		return FALSE;
	}

	if (channelID == CHAN_NONE)
	{
		if (instrumentType == INSTR_FM)
			channelID = CHAN_FM1;
		else if (instrumentType == INSTR_PSG)
			channelID = CHAN_PSG1;
		else if (instrumentType == INSTR_PCM)
			channelID = CHAN_PCM;
	}

	sprintf(string, ";;;;;;;;;; ECHO_SFX\n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);

	unsigned char lockEvent = 0;
	unsigned char noteOffEvent = 0;
	unsigned short setInstrEvent = 0;
	unsigned short setVolumeEvent = 0;
	unsigned short noteOnEvent = 0;
	unsigned int nbTicks = duration *3/50;

	switch(channelID)
	{
		case CHAN_FM1:
			lockEvent = 0xE0;
			noteOffEvent = 0x10;
			setInstrEvent = 0x4000 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2000 | ((0xFF-volume) >> 1);
			noteOnEvent = 0x0000 | note; //mid note
			break;
		case CHAN_FM2:
			lockEvent = 0xE1;
			noteOffEvent = 0x11;
			setInstrEvent = 0x4100 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2100 | ((0xFF-volume) >> 1);
			noteOnEvent = 0x0100 | note; //mid note
			break;
		case CHAN_FM3:
			lockEvent = 0xE2;
			noteOffEvent = 0x12;
			setInstrEvent = 0x4200 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2200 | ((0xFF-volume) >> 1);
			noteOnEvent = 0x0200 | note; //mid note
			break;
		case CHAN_FM4:
			lockEvent = 0xE4;
			noteOffEvent = 0x14;
			setInstrEvent = 0x4400 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2400 | ((0xFF-volume) >> 1);
			noteOnEvent = 0x0400 | note; //mid note
			break;
		case CHAN_FM5:
			lockEvent = 0xE5;
			noteOffEvent = 0x15;
			setInstrEvent = 0x4500 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2500 | ((0xFF-volume) >> 1);
			noteOnEvent = 0x0500 | note; //mid note
			break;
		case CHAN_FM6:
			lockEvent = 0xE6;
			noteOffEvent = 0x16;
			setInstrEvent = 0x4600 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2600 | ((0xFF-volume) >> 1);
			noteOnEvent = 0x0600 | note; //mid note
			break;

		case CHAN_PSG1:
			lockEvent = 0xE8;
			noteOffEvent = 0x18;
			setInstrEvent = 0x4800 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2800 | ((0xFF-volume) >> 4);
			noteOnEvent = 0x0800 | note; //mid note
			break;
		case CHAN_PSG2:
			lockEvent = 0xE9;
			noteOffEvent = 0x19;
			setInstrEvent = 0x4900 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2900 | ((0xFF-volume) >> 4);
			noteOnEvent = 0x0900 | note; //mid note
			break;
		case CHAN_PSG3:
			lockEvent = 0xEA;
			noteOffEvent = 0x1A;
			setInstrEvent = 0x4A00 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2A00 | ((0xFF-volume) >> 4);
			noteOnEvent = 0x0A00 | note; //mid note
			break;
		case CHAN_PSG4:
			lockEvent = 0xEB;
			noteOffEvent = 0x1B;
			setInstrEvent = 0x4B00 | (instrumentID & 0xFF);
			setVolumeEvent = 0x2B00 | ((0xFF-volume) >> 4);
			noteOnEvent = 0x0B00 | note; //mid note
			break;
		case CHAN_PCM:
			lockEvent = 0xE6; //FM6
			noteOffEvent = 0x1C;
			//setInstrEvent = 0; //not for PCM
			//setVolumeEvent = 0; //not for PCM
			noteOnEvent = 0x0C00 | (instrumentID & 0xFF);
			break;
	}


	sprintf(string, "\tdc.b 0x%.2x\t; lock\n", lockEvent);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0x%.2x\t; noteoff\n", noteOffEvent);
	fwrite(string, strlen(string), 1, output);

	if (instrumentType != INSTR_PCM)
	{
		sprintf(string, "\tdc.w 0x%.4x\t; set instrument\n", setInstrEvent);
		fwrite(string, strlen(string), 1, output);

		sprintf(string, "\tdc.w 0x%.4x\t; set volume\n", setVolumeEvent);
		fwrite(string, strlen(string), 1, output);
	}

	sprintf(string, "\tdc.w 0x%.4x\t; noteon\n", noteOnEvent);
	fwrite(string, strlen(string), 1, output);


	while (nbTicks >= 256)
	{
		sprintf(string, "\tdc.w 0xfe00\t; wait 256 ticks\n");
		fwrite(string, strlen(string), 1, output);

		nbTicks -= 256;
	}

	sprintf(string, "\tdc.w 0xfe%.2x\t; wait %d ticks\n", nbTicks &0xFF, nbTicks);
	fwrite(string, strlen(string), 1, output);


	sprintf(string, "\tdc.b 0xff\t; end\n");
	fwrite(string, strlen(string), 1, output);




	return TRUE;
}

////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 echo_sfxExecute(char *info, FILE *output)
{
	char keywordx[9]; //ECHO_SFX\0
	char id[50];
	char file[MAX_PATH];
	char *parameters;
	char use_param = FALSE;
	int nbElem = 0;
	int ret = 0;

	parameters = extractParameters(info);

	ret = sscanf(info, "%s", keywordx);
	if (ret > 0 )	nbElem++;
	info += strlen(keywordx);
	trim(info, EMPTY_CHAR);


	ret = sscanf(info, "%s", id);
	if (ret > 0 )	nbElem++;
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	ret = sscanf(info, "\"%[^\"]\"", file);
	if (ret > 0)	nbElem++;
	if (nbElem < 2)
	{
		printf("Wrong ECHO_SFX definition\n");
		echo_sfxHelp();
		return FALSE;
	}
	else if (nbElem == 3)
	{
		info += (strlen(file) + 2); 	//+2 for doublequote
		trim(info, EMPTY_CHAR);
	}
	else if (nbElem == 2)
	{
		use_param = TRUE;
		if (parameters == NULL)
		{
			printf("Error : Bank info is mandatory\n");
			echo_sfxHelp();
			return FALSE;
		}
	}

	/*
	echo_sfx_options.FMDrive_enabled = FALSE;
	echo_sfx_options.SPSG_enabled = FALSE;

	if ( strlen(info) )
	{
		option = strstr(info, "FMDRIVE");
		if (option != NULL)
		{
			echo_sfx_options.FMDrive_enabled = TRUE;
		}

		option = strstr(info, "SPSG");
		if (option != NULL)
		{
			echo_sfx_options.SPSG_enabled = TRUE;
		}
	}
*/
	if (verbose)
	{
		printf("\n%s found\n", keywordx);	 //ECHO_SFX
		printf("  id    : %s \n", id);
		if (use_param)
			printf("  generate sfx from a bank instrument");
		else
			printf("  song  : %s \n\n", file);
	}

	if (use_param)
	{
		parseBankInfo(parameters);
		return generateSFX(output, id);
	}
	else
	{
		return processFile(output, id, file);
	}
}

void echo_sfxHelp()
{
	printf("ECHO_SFX import a SFX, to play over BGM\n");
	printf("Support complexe (ESF) or single bank play\n");
	printf("See ECHO_BANK for instruments define\n");

	printf("\nBasic usage:\n");
	printf("\tECHO_SFX id \"file.esf\"\n");
	printf("or\n");
	printf("\tECHO_SFX id\n");
	printf("\t{\n");
	printf("\t\t<single instrument>\n");
	printf("\t}\n");

	printf("\nwhere\n\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tSFX to import (ESF)\n");

	printf("\n  Single instrument:\n");
	printf("  - PCM voiceID duration\n");
	printf("\tindex of the PCM voice in Bank\n");
	printf("\tduration of voice in ms\n");
	printf("  - FM voiceID note duration [FMx]\n");
	printf("\tIndex of the FM voice in Bank\n");
	printf("\tnote (0-247)\n");
	printf("\tduration of note in ms\n");
	printf("\tchannel (FM1 default)\n");
	printf("  - PSG voiceID note duration [PSGx]\n");
	printf("\tIndex of the FM voice in Bank\n");
	printf("\tnote (0-142)\n");
	printf("\tduration of note in ms\n");
	printf("\tchannel (PSG1 default)\n");

	printf("\n\nExample (full):\n");
	printf("DAC sega_ewf \"data/Sonic_the_Hedgehog_Sega_opening.wav\" 0 FORMAT_EWF\n");
	printf("ECHO_BANK instruments_pcm\n");
	printf("{\n");
	printf("\tDATA sega_ewf\n");
	printf("}\n");
	printf("ECHO_SFX sega_esf\n");
	printf("{\n");
	printf("\tPCM 0 2350\n");
	printf("}");
}
