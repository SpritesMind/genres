#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/image_utils.h"
#include "utils/parser.h"
#include "utils/packer.h"



void font_convert( FILE *output, char *id  )
{
	char	string[256];

	if (nbTiles < 96 )
	{
		printf("WARNING missing %d characters\n", 96-nbTiles);
	}
	else if (nbTiles > 96 )
	{
		printf("WARNING SGDK's font is 96 characters long, not %d\n", nbTiles);
	}
	
	image_pack_tiles( ALGO_RAW, 0);

	//header
	sprintf(string, ";;;;;;;;;; FONT \n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);

	image_write_packed(output);

	sprintf(string, "\n");
	fwrite(string, strlen(string), 1, output);
}


////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 fontExecute(char *info, FILE *output)
{
	char keyword[5]; //FONT\0
	char id[50];
	char file[MAX_PATH];
	int nbElem = 0;

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	if (nbElem < 3)
	{
		printf("Wrong FONT definition\n");
		fontHelp();
		return FALSE;
	}
	info += (strlen(file) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //FONT
		printf("  id   : %s \n", id);
		printf("  file : %s \n", file);
	}


	if (!image_load_tiles( file, ALPHA_TOPLEFT))
	{
		printf("File not found\n");
		return FALSE;
	}

	font_convert(output, id);

	image_unload();

	return TRUE;
}

void fontHelp()
{
	printf("FONT extract 96 8x8 characters from a 16 or 256 colors bitmap\n");
	printf("Suitable for SGDK's VDP_loadFontData()\n");
	printf("No pal imported\n");
	printf("If you want more control, use BITMAP\n");
	printf("\nBasic usage:\n");
	printf("\tFONT id \"file\"\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tbitmap to import\n");
}
