#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"

#include "utils/file_utils.h"
#include "utils/parser.h"


u8 verbose = FALSE;



static void handleLine( char *line, FILE *output)
{
	char *keyword;

	line = trim(line, EMPTY_CHAR);
	if (strncmp(line, ";", 1) == 0)	return; //skip comment

	keyword = (char *)malloc(strlen(line)+1);
	if (keyword == NULL)	return;

    memset(keyword, 0, strlen(line)+1);

	sscanf(line, "%s", keyword);
	if (strlen(keyword)==0)
    {
        free(keyword);
        return; //skip empty line
    }

	if (strcmp(keyword, "ANI") == 0)
		printf("\nANI is no longer supported.\nUse SPRITE and dedicated code.\n");
	else if (strcmp(keyword, "BITMAP") == 0)
		bitmapExecute(line, output);
	else if (strcmp(keyword, "HIRES") == 0)
		hiresExecute(line, output);
	else if (strcmp(keyword, "DATA") == 0)
		dataExecute(line, output);
	else if (strcmp(keyword, "FONT") == 0)
		fontExecute(line, output);
	else if (strcmp(keyword, "MAP") == 0)
		mapExecute(line, output);
	else if (strcmp(keyword, "PAL") == 0)
		palExecute(line, output);
	else if (strcmp(keyword, "SPRITE") == 0)
		spriteExecute(line, output);
	else if (strcmp(keyword, "SPRITESHEET") == 0)
		spritesheetExecute(line, output);
	else if (strcmp(keyword, "CONTROLS") == 0)
		controlsExecute(line, output);
	else if (strcmp(keyword, "DAC") == 0)
		dacExecute(line, output);
	else if (strcmp(keyword, "PSG") == 0)
		psgExecute(line, output);
	else if (strcmp(keyword, "ECHO_BGM") == 0)
		echo_bgmExecute(line, output);
	else if (strcmp(keyword, "ECHO_SFX") == 0)
		echo_sfxExecute(line, output);
	else if (strcmp(keyword, "ECHO_BANK") == 0)
		echo_bankExecute(line, output);
	else if (strcmp(keyword, "BANK") == 0)
		bankExecute(line, output);
	else if (strcmp(keyword, "MERGE") == 0)
		mergeExecute(line, output);
	else
		printf("Unknown %s\n", keyword);

	free(keyword);
}

static void printHelp( char *resourceType)
{
	char *keyword;

	keyword = trim(resourceType, EMPTY_CHAR);
	if (strlen(keyword)==0)
    {
        return;
    }

	if (strcmp(keyword, "ANI") == 0)
		printf("\nANI is no longer supported.\nUse SPRITE and dedicated code.\n");
	else if (strcmp(keyword, "BITMAP") == 0)
		bitmapHelp();
	else if (strcmp(keyword, "HIRES") == 0)
		hiresHelp();
	else if (strcmp(keyword, "DATA") == 0)
		dataHelp();
	else if (strcmp(keyword, "FONT") == 0)
		fontHelp();
	else if (strcmp(keyword, "MAP") == 0)
		mapHelp();
	else if (strcmp(keyword, "PAL") == 0)
		palHelp();
	else if (strcmp(keyword, "SPRITE") == 0)
		spriteHelp();
	else if (strcmp(keyword, "SPRITESHEET") == 0)
		spritesheetHelp();
	else if (strcmp(keyword, "CONTROLS") == 0)
		controlsHelp();
	else if (strcmp(keyword, "DAC") == 0)
		dacHelp();
	else if (strcmp(keyword, "PSG") == 0)
		psgHelp();
	else if (strcmp(keyword, "ECHO_BGM") == 0)
		echo_bgmHelp();
	else if (strcmp(keyword, "ECHO_SFX") == 0)
		echo_sfxHelp();
	else if (strcmp(keyword, "ECHO_BANK") == 0)
		echo_bankHelp();
	else if (strcmp(keyword, "BANK") == 0)
		bankHelp();
	else if (strcmp(keyword, "MERGE") == 0)
		mergeHelp();
	else
		printf("Unknown resourceType %s\n", keyword);
}


int main(int argc, char *argv[])
{
    FILE *in;
    FILE *out;
    long fileSize;

    struct str_parserInfo *parserInfo;
    long lineCount, i;
	char *text;
	char *line;

    if (argc < 3)
	{
    	//"genres help" gives the same result as "genres" or "genres input.rc"
		printf("\nGenRes - the resource compiler for Sega Megadrive / Genesis\n");
		printf("version 2.1 - %s by KanedaFr\n", __DATE__);
		printf("Use\n  - FreeImage library v%s to convert bitmap\n", FreeImage_GetVersion());
		printf("  - SoX library v%s to convert audio\n", sox_version());
		printf("  - ST-Sound Library v1.43 by Arnaud Carre\n");
		printf("Visit http://gendev.spritesmind.net from more Genny related infos and tools\n");

		printf("\nUsage:\n");
		printf("\tgenres input.rc output.asm [-v]\n");
		printf("\tgenres help [resourceType]\n");

		printf("\nOptions:\n");
		printf("\t -v active verbose output\n");

		printf("\nSupported resourceType :\n");
		printf("\tPAL\n");
		printf("\tBITMAP\n");
		printf("\tHIRES\n");
		printf("\tSPRITE\n");
		printf("\tSPRITESHEET\n");
		printf("\tFONT\n");
		printf("\tMAP\n");
		printf("\tCONTROLS\n");
		printf("\tDATA\n");
		printf("\tDAC\n");
		printf("\tPSG\n");
		printf("\tECHO_BANK\n");
		printf("\tECHO_BGM\n");
		printf("\tECHO_SFX\n");
		printf("\tBANK\n");

		return 0;
	}


    if (strcmp(argv[1], "help") == 0)
    {
    	printHelp(argv[2]);
    	return 0;
    }



    verbose = ( (argc == 4) && (strcmp(argv[3], "-v") == 0) );
	if (verbose)
		printf("Verbose mode : ON\n");


    if (!file_isValid(argv[1]))
        return -1;

    fileSize = file_getSize(argv[1]);
    in = fopen(argv[1], "rt");


	parserInfo = parser_loadFromFile( in, fileSize );
	if (parserInfo == NULL)
	{
		fclose(in);
		if (verbose)	printf("%s\n",parser_error);
		return -1;
	}

	fclose(in);

    if (verbose)    printf("Parsing done\n");




    ////// write output
	out = fopen(argv[2], "wt");
	if (out == NULL)
	{
		printf("Can't create %s", argv[2]);
		parser_unload(parserInfo);
		return -1;
	}

	FreeImage_Initialise(TRUE);

	lineCount = parser_getLinesCount(parserInfo);
	for(i=0; i< lineCount; i++)
	{
		line = (char *) parser_getLine(parserInfo, i);
		if (strlen(line) == 0)  continue;

		//alloc enough for every char + 1 per line
		text = (char *) malloc(fileSize + lineCount);
		if (text == NULL)
		{
			printf("Can't allocate %ld bytes", fileSize + lineCount);
			break;
		}

        memset(text, 0, fileSize + lineCount);

		sprintf(text,"%s", line);


		if (i < lineCount-2)
		{
			line = (char *) parser_getLine(parserInfo, i+1);
			if (strlen(line) == 0)  continue;

			if (line[0] == '{')
			{
				while( (line[0] != '}') && (i < lineCount-1))
				{
					i++;
					line = (char *) parser_getLine(parserInfo, i);
					sprintf(text,"%s\n%s", text, line);
				}
			}
		}

		handleLine( text, out );

		free(text);
	}

	FreeImage_DeInitialise();

	parser_unload(parserInfo);

    return 0;
}
