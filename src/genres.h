#ifndef __GENRES_H__
#define __GENRES_H__

#define FREEIMAGE_LIB
#include <FreeImage.h>

#include <sox.h>


#define TRUE    1
#define FALSE   0

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })



typedef unsigned char  u8;
typedef unsigned short int  u16;
typedef signed short int  s16;
typedef unsigned long  u32;

extern u8 verbose;

#ifdef __cplusplus
extern "C" {
#endif

u8 dataExecute(char *info, FILE *output);
void dataHelp();

u8 controlsExecute(char *info, FILE *output);
void controlsHelp();

u8 palExecute(char *info, FILE *output);
void palHelp();

u8 fontExecute(char *info, FILE *output);
void fontHelp();

u8 bitmapExecute(char *info, FILE *output);
void bitmapHelp();

u8 hiresExecute(char *info, FILE *output);
void hiresHelp();

u8 mergeExecute(char *info, FILE *output);
void mergeHelp();

u8 spriteExecute(char *info, FILE *output);
void spriteHelp();

u8 spritesheetExecute(char *info, FILE *output);
void spritesheetHelp();

u8 mapExecute(char *info, FILE *output);
void mapHelp();

u8 echo_bankExecute(char *info, FILE *output);
void echo_bankHelp();

u8 echo_bgmExecute(char *info, FILE *output);
void echo_bgmHelp();

u8 echo_sfxExecute(char *info, FILE *output);
void echo_sfxHelp();

u8 dacExecute(char *info, FILE *output);
void dacHelp();

u8 psgExecute(char *info, FILE *output);
void psgHelp();



u8 bankExecute(char *info, FILE *output);
void bankHelp();


#ifdef __cplusplus
}
#endif

#endif
