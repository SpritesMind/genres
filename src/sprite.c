#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/image_utils.h"
#include "utils/packer.h"
#include "utils/parser.h"


struct options
{
	u8 exportPal;
	u8 exportPixels;
	char alphaCode;
	u8 keepEmpty;
} sprite_options;

//sprWidth = nb tile width, sprHeight = nb tile height (so between 1 & 4)
//if 0, auto compute
static void sprite_convert( FILE *output, char *id, unsigned char sprWidth, unsigned char sprHeight  )
{
	int	j,k;
	char	string[256];
	int	    curTile, nbSprites;
	int	    size;
	//short int    paddedTileX, paddedTileY;
	short int    nbSpritesX, nbSpritesY;
	short int    curSpriteX, curSpriteY;


	//init all (nbTileX,....)
	image_pack_tiles(ALGO_RAW, 0);
	
	if (sprWidth == 0)
	{
		if (nbTilesX > 4)
		{
			sprWidth = 3;
			if ( (nbTilesX/3) >= (nbTilesX/4) )	sprWidth = 4;

		}
		else
		{
			sprWidth = nbTilesX & 0xff;
		}
	}
	nbSpritesX = nbTilesX / sprWidth;
	//paddedTileX = nbSpritesX*sprWidth;

	if (sprHeight == 0)
	{
		if (nbTilesY > 4)
		{
			sprHeight = 3;
			if ( (nbTilesY/3) >= (nbTilesY/4) )	sprHeight = 4;
		}
		else
		{
			sprHeight =  nbTilesY & 0xFF;
		}
	}
	nbSpritesY = nbTilesY / sprHeight;
	//paddedTileY = nbSpritesY*sprHeight;
	

	//header
	sprintf(string, ";;;;;;;;;; SPRITE \n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);

	if (sprite_options.exportPal)
		sprintf(string, "\tdc.l %s_pal\t; pal data adress\n", id);
	else
		sprintf(string, "\tdc.l 0\t; NO_PAL\n");
	fwrite(string, strlen(string), 1, output);

	if (sprite_options.exportPixels)
		sprintf(string, "\tdc.l %s_sprites\t; sprites adress\n", id);
	else
		sprintf(string, "\tdc.l 0\t; NO_PIXELS\n");
	fwrite(string, strlen(string), 1, output);

	if (sprite_options.exportPixels)
		sprintf(string, "\tdc.w %d\t; nb sprites\n", nbSpritesY * nbSpritesX );
	else
		sprintf(string, "\tdc.w 0\t; nb sprites\n");
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.w %d\t; width\n", sprWidth*8);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.w %d\t; height\n", sprHeight*8);
	fwrite(string, strlen(string), 1, output);

	size = sprWidth-1;
	size <<= 2;
	size |= sprHeight-1;
	// not SGDK compatible
	//size <<= 8;
	sprintf(string, "\tdc.b 0x%.2x\t; MD size\n\n", size);
	fwrite(string, strlen(string), 1, output);

	//pal
	if (sprite_options.exportPal)
	{
		sprintf(string, "\t.align 2\n%s_pal:\n", id);
		fwrite(string, strlen(string), 1, output);

		image_write_pal(output, 0);
	}

	if (sprite_options.exportPixels)
	{
		//sprites' tiles
		sprintf(string, "\n\t.align 2\n%s_sprites:\n", id);
		fwrite(string, strlen(string), 1, output);

		nbSprites = (int) nbSpritesX*nbSpritesY;
		for (j=0; j< nbSprites; j++)
		{
			sprintf(string, "\tdc.l\t%s_sprite%.2d\n", id, j);
			fwrite(string, strlen(string), 1, output);
		}

		sprintf(string, "\n\t.align 2\n");
		fwrite(string, strlen(string), 1, output);

		for(curSpriteY=0; curSpriteY< nbSpritesY; curSpriteY++)
		{
			for(curSpriteX=0; curSpriteX< nbSpritesX; curSpriteX++)
			{
				sprintf(string, "%s_sprite%.2d:\n", id, curSpriteY*nbSpritesX + curSpriteX);
				fwrite(string, strlen(string), 1, output);

				if (sprite_options.exportPixels)
				{
					for(j=0;j<sprWidth;j++)
					{

						for( k=0; k<sprHeight; k++)
						{
							curTile = curSpriteY*sprHeight;
							curTile+= k;
							curTile*= (nbSpritesX*sprWidth);
							
							curTile+= (curSpriteX*sprWidth);
							curTile+= j;

							image_write_tile(output, curTile);

							sprintf(string, "\n");
							fwrite(string, strlen(string), 1, output);
						}
					}
				}
			}
		}
	}

}


////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 spriteExecute(char *info, FILE *output)
{
	char keyword[7]; //SPRITE\0
	char id[50];
	char buffer[10];
	char file[MAX_PATH];
	unsigned char sprWidth = 0;
	unsigned char sprHeight = 0;
	char *option;
	int a=0,b=0;
	int nbElem = 0;

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	info += (strlen(file) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%d", &a);
	info += sprintf ( buffer, "%d", a);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%d", &b);
	info += sprintf ( buffer, "%d", b);
	trim(info, EMPTY_CHAR);

	sprWidth = a &0xff;
	sprHeight = b & 0xff;

	//convert sprite size (pixel vs tile)
	sprWidth = min( sprWidth/8 , 4 );
	sprHeight = min( sprHeight/8, 4 );
	if ( nbElem < 5 )
	{
		printf("Wrong SPRITE definition\n");
		spriteHelp();
		return FALSE;
	}

	//init default
	sprite_options.exportPal = TRUE;
	sprite_options.exportPixels = TRUE;
	sprite_options.keepEmpty = TRUE;
	sprite_options.alphaCode = ALPHA_TOPLEFT;

	if ( strlen(info) )
	{
		option = strstr(info, "NO_PAL");
		if (option != NULL)
		{
			sprite_options.exportPal = FALSE;
		}

		option = strstr(info, "NO_PIXEL");
		if (option != NULL)
		{
			sprite_options.exportPixels = FALSE;
		}

		option = strstr(info, "ALPHA_NONE");
		if (option != NULL)
		{
			sprite_options.alphaCode = ALPHA_NONE;
		}

		option = strstr(info, "ALPHA_BOTTOMRIGHT");
		if (option != NULL)
		{
			sprite_options.alphaCode = ALPHA_BOTTOMRIGHT;
		}

		option = strstr(info, "SKIP_EMPTY");
		if (option != NULL)
		{
			sprite_options.keepEmpty = FALSE;
		}
	}


	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //SPRITE
		printf("  id   : %s \n", id);
		printf("  file : %s \n", file);
		printf("  width: %d \n", sprWidth*8);
		printf("  height:%d \n", sprHeight*8);
		printf("  pal  : %d \n", sprite_options.exportPal);
		printf("  pixel: %d \n", sprite_options.exportPixels);
		//printf("  empty: %d \n", sprite_options.keepEmpty);
		printf("  alpha: %s \n", ((sprite_options.alphaCode == ALPHA_NONE) ? "NONE" : ((sprite_options.alphaCode == ALPHA_TOPLEFT) ? "TopLeft" : "BottomRight")));
	}



	if (!image_load_tiles(file, sprite_options.alphaCode))
	{
		printf("File not found\n");
		return FALSE;
	}

	sprite_convert(output, id, sprWidth, sprHeight );

	image_unload();

	return TRUE;
}

void spriteHelp()
{
	printf("SPRITE convert any 16 or 256 colors bitmap to as much sprite as needed.\n");
	printf("Only the first 16 colors are imported\n");
	printf("For tiles, see BITMAP\n");
	printf("\nBasic usage:\n");
	printf("\tSPRITE id \"file\" width height [options]\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tthe bitmap to convert\n");
	printf("  width\t\twidth of each unique sprite (8,16,24,32)\n");
	printf("  height\theight of each unique sprite (8,16,24,32)\n");

	printf("\n  Options:\n");
	printf("  - NO_PAL\tdoesn't export pal\n");
	printf("  - NO_PIXELS\tdoesn't export tiles, only bitmap info\n");
	//printf("\n  Optimize options:\n");
	//printf("  KEEP_EMPTY\tkeep zero'ed sprite (default)\n");
	//printf("  SKIP_EMPTY\tremove zero'ed sprite\n");
	printf("\n  Alpha options:\n");
	printf("  - ALPHA_TOPLEFT\tuse pixel at top left as transparent color (default)\n");
	printf("  - ALPHA_BOTTOMRIGHT\tuse pixel at top left as transparent color\n");
	printf("  - ALPHA_NONE\t\tkeep color 0 as transparent color\n\n");

	printf("\nExample:\n");
	printf("SPRITE sonic \"resource\\sonic_sheet.bmp\" 16 16 ALPHA_TOPLEFT");
}
