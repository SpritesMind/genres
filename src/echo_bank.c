#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/file_utils.h"
#include "utils/parser.h"


#define TYPE_FM		1
#define TYPE_PSG	2
#define TYPE_PCM	3
#define TYPE_DATA	4

struct options
{
	int	 frameWidth;
} echo_bank_options;

//y12
struct ym2612op {
	unsigned char op_data[8];
	long op_padding[2];
};

struct ym2612voice {
	struct ym2612op		op[4];
	unsigned char		algo;
	unsigned char		fback;
	unsigned char 		padding[14];
};

struct ym2612_KMod {
	struct ym2612voice	ym;
	unsigned char		name[16];
	unsigned char		dumper[16];
	unsigned char		game[16];
};

//tfi
struct tfi_op {
	unsigned char multiple;//0..15
	unsigned char detune;//-3..0..3
	unsigned char totalLevel;//0..127
	unsigned char rateScale;//0..3
	unsigned char attack;//0..31
	unsigned char decay;//0..31
	unsigned char sustain;//0..31
	unsigned char release;//0..15
	unsigned char sustainLevel;//0..15
	unsigned char ssg;//7..15
};

struct tfi_TFMM {
	unsigned char algo;//0..7
	unsigned char feedback;//0..7
	struct tfi_op op[4];
};


//vgi
struct vgi_op {
	unsigned char multiple;//0..15
	unsigned char detune;//-3..0..3
	unsigned char totalLevel;//0..127
	unsigned char rateScale;//0..3
	unsigned char attack;//0..31
	unsigned char decayAM;//0..31
	unsigned char sustain;//0..31
	unsigned char release;//0..15
	unsigned char sustainLevel;//0..15
	unsigned char ssg;//7..15
};

struct vgi_VGMM {
	unsigned char algo;//0..7
	unsigned char feedback;//0..7
	unsigned char fmsAms;
	struct vgi_op op[4];
};

//tyi
struct tyi_Teedo {
	unsigned char dtmul[4];
	unsigned char tl[4];
	unsigned char rsar[4];
	unsigned char amdr[4];
	unsigned char sr[4];
	unsigned char slrr[4];
	unsigned char ssg[4];
	unsigned char fbalgo;
	unsigned char ms;
	unsigned char sig[2];
};

struct eif_echo {
	unsigned char fbalgo;
	unsigned char dtmul[4];
	unsigned char tl[4];
	unsigned char ar[4];
	unsigned char dr[4];
	unsigned char sr[4];
	unsigned char slrr[4];
	unsigned char ssg[4];
};



struct instrInfo
{
	u8 type;
	char filename[MAX_PATH - 20];//-20 to keep space for sprintf("xxx %s xxx", filename)
									//resourceID for DATA
};

typedef struct {
  struct instrInfo **array;
  size_t used;
  size_t size;
}InstrArray;

InstrArray *instrumentList;


void initInstrArray(InstrArray *a, size_t initialSize)
{
  a->array = (struct instrInfo **)malloc(initialSize * sizeof(struct instrInfo *));
  a->used = 0;
  a->size = initialSize;
}

void insertInstrArray(InstrArray *a, struct instrInfo *info)
{
  if (a->used == a->size) {
    a->size *= 2;
	a->array = (struct instrInfo **)realloc(a->array, a->size * sizeof(struct instrInfo *));
  }
  a->array[a->used++] = info;
}

void freeInstrArray(InstrArray *a)
{
  size_t i;
  for (i=0; i < a->used; i++)
	  free( a->array[i] );

  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}


static struct instrInfo *handleLine(char *text)
{
	struct instrInfo *info;
	int items_read;
	int nbChar;

	char *ptr = malloc( strlen(text)+1 );
	char *savePtr = ptr;

    memset(ptr, 0, strlen(text)+1);
    memcpy(ptr, text, strlen(text));


	info = (struct instrInfo *) malloc(sizeof(struct instrInfo));
	if ( strncmp(ptr, "FM" , strlen("FM")) == 0 )
	{
		ptr += strlen("FM");
		info->type = TYPE_FM;
	}
	else if(strncmp(ptr, "PSG", strlen("PSG")) == 0)
	{
		ptr += strlen("PSG");
		info->type = TYPE_PSG;
	}
	else if (strncmp(ptr, "PCM", strlen("PCM")) == 0)
	{
		ptr += strlen("PCM");
		info->type = TYPE_PCM;
	}
	else if (strncmp(ptr, "DATA", strlen("DATA")) == 0)
	{
			ptr += strlen("DATA");
			info->type = TYPE_DATA;
	}
	else
	{
		free(info);
		free(savePtr);
		return NULL;
	}

	trim(ptr, EMPTY_CHAR);

	if (info->type == TYPE_DATA)
	{
		items_read = sscanf(ptr, "%s", info->filename);
		if (items_read != 1)
		{
			printf("unable to read resourceID from %s, %d\n", ptr, items_read);
			free(info);
			free(savePtr);
			return NULL;
		}
	}
	else
	{
		items_read = sscanf(ptr, "\"%[^\"]\"%n", info->filename, &nbChar);
		if (items_read != 1)
		{
			printf("unable to find file name from %s, %d\n", ptr, items_read);
			free(info);
			free(savePtr);
			return NULL;
		}

		if (file_isValid(info->filename) == FALSE)
		{
			free(info);
			free(savePtr);
			return NULL;
		}
	}
	//ptr += nbChar;
	//trim(ptr, EMPTY_CHAR);

    free(savePtr);

	return info;
}


u8 parseInstruments(char *text)
{
	long i, nbLines;

	struct str_parserInfo *instrParseInfo;
	instrParseInfo = parser_load(text);
	if (instrParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

    nbLines = parser_getLinesCount(instrParseInfo);
	for (i = 0; i< nbLines; i++)
	{
		struct instrInfo *info = handleLine((char *)parser_getLine(instrParseInfo, i));
		//if (info != NULL)
			insertInstrArray(instrumentList, info);
	}
	parser_unload(instrParseInfo);

	return TRUE;
}



static void processFM(FILE *output, char *filename)
{
	u8 op;
	char	string[MAX_PATH];
	FILE	*instrFile;
	long	instrFileSize;
	char	*ext;
	struct eif_echo eif;

	memset(&eif, 0, sizeof(struct eif_echo));

	if (!file_isValid(filename))    return;

	ext = file_getExtension(_strlwr(filename));
	instrFileSize = file_getSize(filename);

	instrFile = fopen(filename, "rb");
	if (strncmp(ext, "eif", strlen("eif")) == 0)
	{
		fread(&eif, sizeof(struct eif_echo), 1, instrFile);
	}
	else if(strncmp(ext, "tfi", strlen("tfi")) == 0)
	{
		//from https://github.com/sikthehedgehog/mdtools/blob/master/tfi2eif/tool/eif.c
		struct tfi_TFMM	tfi;
		static u8 detune_table[] =	{ 0x07, 0x06, 0x05, 0x00, 0x01, 0x02, 0x03 };

		fread(&tfi, sizeof(struct tfi_TFMM), 1, instrFile);
		//ReadFile(instrFile, &tfi, sizeof(struct tfi_TFMM), &dwByteWritten, NULL);
		eif.fbalgo = (tfi.feedback << 3) | tfi.algo;
		for (op = 0; op < 4; op++)
		{
			eif.dtmul[op] = (detune_table[tfi.op[op].detune] << 4) | tfi.op[op].multiple;
			eif.tl[op] = tfi.op[op].totalLevel;
			eif.ar[op] = (tfi.op[op].rateScale << 6) | tfi.op[op].attack;
			eif.dr[op] = tfi.op[op].decay;
			eif.sr[op] = tfi.op[op].sustain;
			eif.slrr[op] = (tfi.op[op].sustainLevel << 4) | tfi.op[op].release;
			eif.ssg[op] = tfi.op[op].ssg;
		}
		//TODO test
	}
	else if (strncmp(ext, "vgi", strlen("vgi")) == 0)
	{
		//from https://github.com/sikthehedgehog/mdtools/blob/master/vgi2eif/tool/eif.c
		struct vgi_VGMM	vgi;
		static u8 detune_table[] = { 0x07, 0x06, 0x05, 0x00, 0x01, 0x02, 0x03 };

		fread(&vgi, sizeof(struct vgi_VGMM), 1, instrFile);
		eif.fbalgo = (vgi.feedback << 3) | vgi.algo;
		for (op = 0; op < 4; op++)
		{
			eif.dtmul[op] = (detune_table[vgi.op[op].detune] << 4) | vgi.op[op].multiple;
			eif.tl[op] = vgi.op[op].totalLevel;
			eif.ar[op] = (vgi.op[op].rateScale << 6) | vgi.op[op].attack;
			eif.dr[op] = vgi.op[op].decayAM;
			eif.sr[op] = vgi.op[op].sustain;
			eif.slrr[op] = (vgi.op[op].sustainLevel << 4) | vgi.op[op].release;
			eif.ssg[op] = vgi.op[op].ssg;
		}
		//TODO test
	}
	else if (strncmp(ext, "tyi", strlen("tyi")) == 0)
	{
		//from https://github.com/vgmtool/vgm2pre/blob/master/vgm2pre/fmt/tyi.hpp
		struct tyi_Teedo tyi;

		fread(&tyi, sizeof(struct tfi_TFMM), 1, instrFile);
		eif.fbalgo = tyi.fbalgo;
		for (op = 0; op < 4; op++)
		{
			eif.dtmul[op] = tyi.dtmul[op];
			eif.tl[op] = tyi.tl[op];
			eif.ar[op] = tyi.rsar[op];
			eif.dr[op] = tyi.amdr[op];
			eif.sr[op] = tyi.sr[op];
			eif.slrr[op] = tyi.slrr[op];
			eif.ssg[op] = tyi.ssg[op];
		}
		//TODO test
	}
	else if (strncmp(ext, "fmd", strlen("fmd")) == 0)
	{
		//42 decimal value, comma delimited with
		//ALGO, FB,
		//MUL,DT,TL,RS,AR,DR,D2R,RR,SL,SSG
		//MUL,DT,TL,RS,AR,DR,D2R,RR,SL,SSG
		//MUL,DT,TL,RS,AR,DR,D2R,RR,SL,SSG
		//MUL,DT,TL,RS,AR,DR,D2R,RR,SL,SSG

		char fmdString[MAX_PATH];
		unsigned char fmdData[42];
		unsigned char idx = 0;
		char *fmdValue;

		fread(fmdString, instrFileSize, 1, instrFile);
		fmdValue = strtok(fmdString, ",");
		while ( (fmdValue != NULL) && (idx < 42)) {
			fmdValue = strtok(NULL, ",");
			fmdData[idx++] = atoi(fmdValue);
		}

		eif.fbalgo = (fmdData[1] << 3) | fmdData[0];
		for (op = 0; op < 4; op++)
		{
			idx = op * 10 + 2;
			eif.dtmul[op] = (fmdData[idx + 1] << 4) | fmdData[idx + 0];
			eif.tl[op] = fmdData[idx + 2];
			eif.ar[op] = (fmdData[idx + 3] << 6) | fmdData[idx + 4];
			eif.dr[op] = fmdData[idx + 5];
			eif.sr[op] = fmdData[idx + 6];
			eif.slrr[op] = (fmdData[idx + 8] << 4) | fmdData[idx + 7];
			eif.ssg[op] = fmdData[idx + 9];
		}
		//TODO test
	}
	else if (strncmp(ext, "y12", strlen("y12")) == 0)
	{
		//from GensKMod source
		struct ym2612_KMod	y12;

		fread(&y12, sizeof(struct ym2612_KMod), 1, instrFile);
		eif.fbalgo = (y12.ym.fback << 3) | y12.ym.algo;
		for (op = 0; op < 4; op++)
		{
			eif.dtmul[op] = y12.ym.op[op].op_data[0];
			eif.tl[op] = y12.ym.op[op].op_data[1];
			eif.ar[op] = y12.ym.op[op].op_data[2];
			eif.dr[op] = y12.ym.op[op].op_data[3];
			eif.sr[op] = y12.ym.op[op].op_data[4];;
			eif.slrr[op] = y12.ym.op[op].op_data[5];
			eif.ssg[op] = y12.ym.op[op].op_data[6];
		}
		//TODO test
	}
	else
	{
		printf("Unsupported FM instrument %s\n", ext);

        fclose(instrFile);
		return;
	}
    fclose(instrFile);

	sprintf(string, "\tdc.b 0x%.2X \t\t\t\t\t;-- Algorithm and feedback\n", eif.fbalgo);
    fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0x%.2X,0x%.2X,0x%.2X,0x%.2X \t;-- Multiplier and detune\n", eif.dtmul[0], eif.dtmul[1], eif.dtmul[2], eif.dtmul[3]);
    fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0x%.2X,0x%.2X,0x%.2X,0x%.2X \t;-- Total level\n", eif.tl[0], eif.tl[1], eif.tl[2], eif.tl[3]);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0x%.2X,0x%.2X,0x%.2X,0x%.2X \t;-- Attack rate\n", eif.ar[0], eif.ar[1], eif.ar[2], eif.ar[3]);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0x%.2X,0x%.2X,0x%.2X,0x%.2X \t;-- Decay rate\n", eif.dr[0], eif.dr[1], eif.dr[2], eif.dr[3]);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0x%.2X,0x%.2X,0x%.2X,0x%.2X \t;-- Sustain rate\n", eif.sr[0], eif.sr[1], eif.sr[2], eif.sr[3]);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0x%.2X,0x%.2X,0x%.2X,0x%.2X \t;-- Release rate and sustain level\n", eif.slrr[0], eif.slrr[1], eif.slrr[2], eif.slrr[3]);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.b 0x%.2X,0x%.2X,0x%.2X,0x%.2X \t;-- SSG-EG\n", eif.ssg[0], eif.ssg[1], eif.ssg[2], eif.ssg[3]);
	fwrite(string, strlen(string), 1, output);
}

static void processPSG(FILE *output, char *filename)
{
	char    string[MAX_PATH];
	char    *ext;

    if (!file_isValid(filename))    return;

	ext = file_getExtension(_strlwr(filename));
	if (strncmp(ext, "psg", strlen("psg")) == 0)
	{
		sprintf(string, "\tINCBIN \"%s\"\n", filename);
		fwrite(string, strlen(string), 1, output);
        //WriteFile(output, string, strlen(string), &dwByteWritten, 0);
	}
	else
	{
		printf("Unsupported PSG format %s\n", ext);
		return;
	}
}

static void processPCM(FILE *output, char *filename)
{
	char	string[MAX_PATH];
	char	*ext;

    if (!file_isValid(filename))    return;

	ext = file_getExtension(_strlwr(filename));
	if (strncmp(ext, "ewf", strlen("ewf")) == 0)
	{
		sprintf(string, "\tINCBIN \"%s\"\n", filename);
		fwrite(string, strlen(string), 1, output);
        //WriteFile(output, string, strlen(string), &dwByteWritten, 0);
	}
	else
	{
		//TODO : sOx
		printf("Unsupported PCM format %s\n", ext);
	}
}


static void echoInstr_process(FILE *output, char *id) //, int frameWidth, int frameHeight)
{
	int	j;
	char	string[256];

	//header
	sprintf(string, ";;;;;;;;;; ECHO_BANK \n\t.align 2\n\t.globl %s\n%s:\n", id, id);
    fwrite(string, strlen(string), 1, output);

	for (j=0; j < instrumentList->used; j++)
	{
		struct instrInfo *info = instrumentList->array[j];
		if ( (info != NULL) && (info->type == TYPE_DATA))
			sprintf(string, "\tdc.l\t%s\n", info->filename);
		else
			sprintf(string, "\tdc.l\t%s_instr%.2d\n", id, j);
        fwrite(string, strlen(string), 1, output);
	}
	sprintf(string, "\tdc.l\t0\n");
    fwrite(string, strlen(string), 1, output);

	for (j = 0; j < instrumentList->used; j++)
	{
		struct instrInfo *info = instrumentList->array[j];

		if (info == NULL)
		{
			printf("\n\t.align 2\n%s_instr%.2d:\t; ERROR\n\tdc.w 0\n", id, j); //.w for alignment
            fwrite(string, strlen(string), 1, output);
			continue; //instrument on error so empty stuff
		}
		if (info->type == TYPE_DATA)
			continue;

		sprintf(string, "\n\t.align 2\n%s_instr%.2d:", id, j);
		fwrite(string, strlen(string), 1, output);

		sprintf(string, "\t; %s\n", info->filename);
        fwrite(string, strlen(string), 1, output);

		switch(info->type)
		{
			case TYPE_FM:
				processFM(output, info->filename);
				break;
			case TYPE_PSG:
				processPSG(output, info->filename);
				break;
			case TYPE_PCM:
				processPCM(output, info->filename);
				break;
			case TYPE_DATA:
				//nothing
				break;

		}
	}
}



static u8 processFile( FILE *output, char *id, char *filename )
{
	echoInstr_process(output, id);

	freeInstrArray(instrumentList);

	return TRUE;
}



////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 echo_bankExecute(char *info, FILE *output)
{
	char keyword[10]; //ECHO_BANK\0
	char id[50];
	char file[MAX_PATH];
	char *instruments;
	int nbElem = 0;


	instruments = extractParameters(info);

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);


	if ( nbElem < 2 )
	{
		printf("Wrong ECHO_BANK definition\n");
		echo_bankHelp();
		return FALSE;
	}

	instrumentList = (InstrArray *)malloc(sizeof(InstrArray));
	initInstrArray(instrumentList, 1);
	if (instruments != NULL)
		parseInstruments(instruments);

	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //echo_bankUMENTS
		printf("  id   : %s \n", id);
	}

	return processFile(output, id, file);
}

void echo_bankHelp()
{
	printf("ECHO_BANK create the instruments list needed by Echo\n");
	printf("To use with echo driver's echo_init( (const void **) bank)\n");
	printf("Convert every known ym2612 instrument to echo instrument\n");
	printf("\nBasic usage:\n");
	printf("\tECHO_BANK id\n");
	printf("\t{\n");
	printf("\t\t<instrument>\n");
	printf("\t\t...\n");
	printf("\t}\n");

	printf("\nwhere\n");

	printf("\n  id\t\tresource name\n");

	printf("\n  Instruments:\n");
	printf("  - FM  filename\timport eif, tfi, vgi, tyi, fmd, y12 file\n");
	printf("  - PSG filename\timport psg file (raw psg data)\n");
	printf("  - PCM filename\timport ewf file\n");
	printf("  - DATA resourceID\tlink to another genres resource (see DAC)\n\n");

	printf("\nExample:\n");

	printf("ECHO_BANK waves_instruments\n");
	printf("{\n");
	printf("\tPSG \"data/psg/PSGFlat.psg\"\n");
	printf("\tFM  \"data/fm/dguitar.eif\"\n");
	printf("\tPCM \"data/pcm/snare.ewf\"\n");
	printf("}");

}
