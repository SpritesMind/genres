*----------------------------------------------------------------------------
* unpack.inc
* VRAM data decompression routine
*
* by Charles MacDonald
* WWW: http://cgfm2.emuviews.com
* modified for XGCC by Kaneda
* modified for return value by Kaneda
*----------------------------------------------------------------------------

* m68k calling convention says that, between function calls,
* d0,d1,a0,a1 can be used as scratch registers,
* however, others should be preserved
* return value -if there is- is placed in d0

* C call :
* 	uint unpack(ulong compressed_data_adr, ulong destination_adr, uint size_of_compressed_data)
* 
* Example :
*	unpack(myCmp, GFX_WRITE_ADDR(32*4), 45)
*	decompress myCmp (45 bytes) at tile 4
*
* Parameters:
*	8(a6) : ulong compressed_data_adr (ushort *data)
*	12(a6): ulong destination_adr
*	16(a6): uint size_of_compressed_data
*
* Input parameters:
* d4 = Data size minus one (saved)
* a4 = Compressed data (not saved)
*
* Output data:
* d0 = number of tiles unpacked
*
* Register use:
* d1 = Run length counter
* d2 = Shift count
* d3 = Pixel buffer
* d5 = Compressed bytes / data
* a0 = VDP data port
*
	.globl unpackRLE
unpackRLE:
			link.w	%a6,#-4
			movem.l  %d2-%d7/%a2-%a4,-(%sp)
			move.l	8(%a6),%a4
			move.l  16(%a6),%d4
			move.l  12(%a6),0xC00004
			
                        move.l	#0, %d0
                        move.l  #0xC00000, %a0
                        move.l  #28, %d2
                        moveq   #0, %d3
        unpack_next:    moveq   #0, %d5
                        move.b  (%a4)+, %d5
                        move.l  %d5, %d1
                        lsr.b   #4, %d1
                        andi.b  #0x0F, %d1
                        andi.b  #0x0F, %d5
        unpack_run:     lsl.l   %d2, %d5
                        or.l    %d5, %d3         
                        lsr.l   %d2, %d5
                        subq.b  #4, %d2
                        bpl     unpack_skip
                        move.l  %d3, (%a0)
                        addq.l	#1, %d0
                        moveq   #0, %d3
                        move.b  #28, %d2
        unpack_skip:    dbra    %d1, unpack_run
                        dbra    %d4, unpack_next 
                        
                        lsr			#3, %d0
        								movem.l (%sp)+,%d2-%d7/%a2-%a4
                        unlk	%a6
			rts
