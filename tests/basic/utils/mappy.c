#include <genesis.h>

#include "genres_sample.h"

#include "utils/mappy.h"
#include "utils/unpack.h"

void mappy_Load( struct mappyLevel *mLevel)
{
	VDP_setPalette(mLevel->idxPal, mLevel->data->pal);
	
	if ( mLevel->data->compSize )
		unpackRLE( mLevel->data->tiles, GFX_WRITE_VRAM_ADDR(mLevel->idxTile*32), mLevel->data->compSize);
	else		
		VDP_doVRamDMA( (u32) mLevel->data->tiles, mLevel->idxTile*32, (32*mLevel->data->nbTiles)/2 );
}

void mappy_Draw( struct mappyLevel *mLevel)
{
	u16 x, y;
	u16	*curMap;
	
	curMap = (u16 *) mLevel->data->layers[ mLevel->idxLevel ];
	for(y=0; y < MIN(mLevel->data->height, 30); y++)
	{
		for(x=0; x< MIN(mLevel->data->width, 48); x++)
		{
			VDP_setTileMapXY(mLevel->fieldAdr, TILE_ATTR_FULL(mLevel->idxPal, 0, 0, 0, (mLevel->idxTile+curMap[y*mLevel->data->width + x])), x, y);
		}
	}
}

void mappy_ScrollH( struct mappyLevel *mLevel, short value)
{
	if (mLevel->data->width*8 < VDP_getScreenWidth())
	{
		mLevel->scrollH = 0;
	}
	else if (value<0)
	{
		mLevel->scrollH = MAX( mLevel->scrollH+value, 0);
	}
	else
	{
		mLevel->scrollH = MIN( mLevel->scrollH+value, mLevel->data->width*8-VDP_getScreenWidth() );
	}
}

void mappy_ScrollV( struct mappyLevel *mLevel, short value)
{
	if (mLevel->data->height*8 < VDP_getScreenHeight())
	{
		mLevel->scrollV = 0;
	}
	else if (value<0)
	{
		mLevel->scrollV = MAX( mLevel->scrollV+value, 0);
	}
	else
	{
		mLevel->scrollV = MIN( mLevel->scrollV+value, mLevel->data->height*8-VDP_getScreenHeight() );
	}
}

void mappy_Update( struct mappyLevel *mLevel)
{
	short x, y;
	u16 posx, posy;
	u16 fakeC47, fakeC48;
	u16 fakeL29, fakeL30;
	u16 scrollTileH, scrollTileV;
	u16	*curMap;

	//note : this will cause garbage on hidden row/col for too small map
	//	but we don't care since it's hidden
	//	+ in real game, we won't call mappy_Update if not needed
	curMap = (u16 *) mLevel->data->layers[ mLevel->idxLevel ];
	
	scrollTileH = mLevel->scrollH / 8;
	fakeC47 = (scrollTileH + 47)%64;
	fakeC48 = (scrollTileH + 48)%64;
	
	scrollTileV = mLevel->scrollV / 8;
	fakeL29 = (scrollTileV + 29)%32;
	fakeL30 = (scrollTileV + 30)%32;

	x = MIN(scrollTileH, 16) * -1;
	for (; x<48; x++)
	{
		posx = scrollTileH + x;
		VDP_setTileMapXY(mLevel->fieldAdr, TILE_ATTR_FULL(mLevel->idxPal, 0, 0, 0, (mLevel->idxTile+curMap[ (scrollTileV+29)*mLevel->data->width + posx])), posx%64, fakeL29);
		VDP_setTileMapXY(mLevel->fieldAdr, TILE_ATTR_FULL(mLevel->idxPal, 0, 0, 0, (mLevel->idxTile+curMap[ (scrollTileV- 2)*mLevel->data->width + posx])), posx%64, fakeL30);
	}
	
	y = MIN( 2, scrollTileV) * -1;
	for (; y<30; y++)
	{
		posy = scrollTileV+y;
		VDP_setTileMapXY(mLevel->fieldAdr, TILE_ATTR_FULL(mLevel->idxPal, 0, 0, 0, (mLevel->idxTile+curMap[ posy*mLevel->data->width + scrollTileH+47])), fakeC47, posy%32);
		VDP_setTileMapXY(mLevel->fieldAdr, TILE_ATTR_FULL(mLevel->idxPal, 0, 0, 0, (mLevel->idxTile+curMap[ posy*mLevel->data->width + scrollTileH-16])), fakeC48, posy%32);
	}
}
