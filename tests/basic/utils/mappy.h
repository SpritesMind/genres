#ifndef _INC_MAPPY_
#define _INC_MAPPY_

#include <genres.h>

struct mappyLevel{
	u8  idxLevel;
	u8	idxPal;
	u16	idxTile;
	u16  fieldAdr;
	u16	scrollV, scrollH;
	genresMap_t *data;
};

void mappy_Load( struct mappyLevel *mLevel);
void mappy_Draw( struct mappyLevel *mLevel);
void mappy_ScrollH( struct mappyLevel *mLevel, short value);
void mappy_ScrollV( struct mappyLevel *mLevel, short value);
void mappy_Update( struct mappyLevel *mLevel);

#endif
