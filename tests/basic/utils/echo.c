#include <genesis.h>
#include <z80_ctrl.h>

#include "utils/echo.h"

extern const u8 echo_driver[1];
extern const u32 echo_driver_size;

void echo_loadDriver()
{
	Z80_clear(0, Z80_RAM_LEN, FALSE);
	Z80_loadCustomDriver(echo_driver, echo_driver_size);
}


// Z80 addresses
/*
static volatile u8*  const z80_ram    = (u8 *)  0xA00000;
static volatile u16* const z80_busreq = (u16 *) 0xA11100;
static volatile u16* const z80_reset  = (u16 *) 0xA11200;
*/

#define Z80_RESET() \
   { 	Z80_startReset(); \
   	   Z80_releaseBus(); \
   	   while(Z80_isBusTaken()); \
   	   Z80_endReset(); }

#define waitEcho()\
		{ \
while ( *pb != 0x00) { \
      Z80_releaseBus(); \
      s16 i; \
      for (i = 0x3FF; i >= 0; i--); \
      Z80_requestBus(1); \
   } \
}

//***************************************************************************
// echo_init
// Initializes Echo and gets it running.
//---------------------------------------------------------------------------
// param list: pointer to instrument list
//***************************************************************************

void echo_init(const void **list) {
	vu8* pb;

   // Take over the Z80
   //Z80_RESET();
   Z80_requestBus(1);

   // Tell Echo to not run any commands by default (the assembly counterpart
   // would tell it to load the instrument list, but we can't do that here
   // due to linker shenanigans)
   pb = (u8*) (Z80_RAM_END);
   *pb = 0x00;

   // Load the instrument list manually, since thanks to linker shenanigans
   // we can't implement the list properly in ROM :/
   //volatile u *dest = &z80_ram[0x1C00];
   pb = (u8*) (Z80_RAM + 0x1C00);

   while (*list) {
      // Retrieve pointer to next instrument
      // Cast it to an integer since we need to treat it as such
      // This should be considered bad C, but since this is hardware-specific
      // code this should be fine to do (portability is not expected)
      u32 ptr = (u32) *list;

      // Turn the pointer into the base+address notation Echo wants and store
      // it in Z80 RAM directly (where the list would go)
      pb[0x000] = (ptr >> 8 & 0x7F) | 0x80;
      pb[0x100] = (ptr & 0xFF);
      pb[0x200] = (ptr >> 15 & 0x7F) | (ptr >> 16 & 0x80);

      // Go for next pointer
      list++;
      pb++;
   }

   // Let Echo start running!
   Z80_RESET();
   Z80_releaseBus();
}

//***************************************************************************
// echo_send_command
// Sends a raw command to Echo. No parameters are taken.
//---------------------------------------------------------------------------
// param cmd: command to send
//***************************************************************************

void echo_send_command(u8 cmd) {
	vu8* pb;
	pb = (u8*) (Z80_RAM_END);

   // We need access to Z80 bus
   Z80_requestBus(1);

   // Is Echo busy yet?
   waitEcho();
   /*
   while ( *pb != 0x00) {
      Z80_releaseBus();
      u16 i;
      for (i = 0x3FF; i >= 0; i--);
      Z80_requestBus(1);
   }
   */

   // Write the command
   *pb = cmd;

   // Done with the Z80
   Z80_releaseBus();
}

//***************************************************************************
// echo_send_command_addr
// Sends a raw command to Echo. An address parameter is taken.
//---------------------------------------------------------------------------
// param cmd: command to send
// param addr: address parameter
//***************************************************************************

void echo_send_command_addr(u8 cmd, const void *addr) {
	vu8* pb;
	pb = (u8*) (Z80_RAM_END);

   // Since we need to split the address into multiple bytes we put it in an
   // integer. This is a bad practice in general, period, but since we don't
   // care about portability here we can afford to do it this time.
   u32 param = (u32) addr;

   // We need access to Z80 bus
   Z80_requestBus(1);

   // Is Echo busy yet?
   waitEcho();
   /*
   while (z80_ram[0x1FFF] != 0x00) {
      Z80_releaseBus();
      int16_t i;
      for (i = 0x3FF; i >= 0; i--);
      Z80_requestBus(1);
   }*/

   // Write the command
   pb = (u8*) (Z80_RAM_END-3);
   pb[1] = param;
   param >>= 8;
   pb[2] = param | 0x80;
   param >>= 7;
   param = (param & 0x7F) | (param >> 1 & 0x80);
   pb[0] = param;

   pb[3] = cmd;

   // Done with the Z80
   Z80_releaseBus();
}

//***************************************************************************
// echo_send_command_byte
// Sends a raw command to Echo. A byte parameter is taken.
//---------------------------------------------------------------------------
// param cmd: command to send
// param byte: parameter
//***************************************************************************

void echo_send_command_byte(u8 cmd, u8 byte) {
	vu8* pb;
	pb = (u8*) (Z80_RAM_END);
   // We need access to Z80 bus
   Z80_requestBus(1);

   // Is Echo busy yet?
   waitEcho();
   /*
   while (z80_ram[0x1FFF] != 0x00) {
      Z80_releaseBus();
      int16_t i;
      for (i = 0x3FF; i >= 0; i--);
      Z80_requestBus(1);
   }
   */

   // Write the command
   pb = (u8*) (Z80_RAM_END-3);
   pb[0] = byte;
   pb[3] = cmd;

   // Done with the Z80
   Z80_releaseBus();
}

//***************************************************************************
// echo_play_bgm
// Starts playing background music.
//---------------------------------------------------------------------------
// param ptr: pointer to BGM stream
//***************************************************************************

void echo_play_bgm(const void *ptr) {
   echo_send_command_addr(ECHO_CMD_PLAYBGM, ptr);
}

//***************************************************************************
// echo_stop_bgm
// Stops background music playback.
//***************************************************************************

void echo_stop_bgm(void) {
   echo_send_command(ECHO_CMD_STOPBGM);
}

//***************************************************************************
// echo_resume_bgm
// Resumes background music playback.
//***************************************************************************

void echo_resume_bgm(void) {
   echo_send_command(ECHO_CMD_RESUMEBGM);
}

//***************************************************************************
// echo_play_sfx
// Starts playing a sound effect.
//---------------------------------------------------------------------------
// param ptr: pointer to SFX stream
//***************************************************************************

void echo_play_sfx(const void *ptr) {
   echo_send_command_addr(ECHO_CMD_PLAYSFX, ptr);
}

//***************************************************************************
// echo_stop_sfx
// Stops sound effect playback.
//***************************************************************************

void echo_stop_sfx(void) {
   echo_send_command(ECHO_CMD_STOPSFX);
}

//***************************************************************************
// echo_set_pcm_rate
// Changes the playback rate of PCM.
//---------------------------------------------------------------------------
// param rate: new rate (timer A value)
//***************************************************************************

void echo_set_pcm_rate(u8 rate) {
   echo_send_command_byte(ECHO_CMD_SETPCMRATE, rate);
}

//***************************************************************************
// echo_get_status
// Retrieves Echo's current status.
//---------------------------------------------------------------------------
// return: status flags (see ECHO_STAT_*)
//***************************************************************************

u16 echo_get_status(void) {
	vu8* pb;
	pb = (u8*) (Z80_RAM + 0x1FF0);
   // We need access to the Z80
   Z80_requestBus(1);

   // Retrieve status from Z80 RAM
   u16 status = 0;
   status = *pb; //z80_ram[0x1FF0];

   pb = (u8*) (Z80_RAM_END);
   if ( *pb != 0)
      status |= ECHO_STAT_BUSY;

   // Done with the Z80
   Z80_releaseBus();

   // Return status
   return status;
}

