#include <genesis.h>
#include <genres.h>
#include <KDebug.h>
#include "genres_sample.h"
#include "level/level.h"

volatile u8 vflag;


void VBlank_sub( )
{
	vtimer++;

	switch(vflag)
	{
		case VGAME:
			Level_Update( );
			break;
	}
}

/***
 * 
 * Main stuff.
 * 
 * Do nothing than initialize main data and launch the loop.
 * 
 * */

int main( )
{
	vflag = VNOTHING;
	JOY_init();
	VDP_waitVSync();

	Level_Init( LEVEL_LOGO, 1 );
	vflag = VGAME;

	SYS_setVIntCallback( &VBlank_sub );

	while(1)
	{
		VDP_waitVSync();
	}
	return (0);
}

