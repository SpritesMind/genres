#ifndef __INC_GENRES_SAMPLE__
#define __INC_GENRES_SAMPLE__

#ifdef DEBUG
#include <kdebug.h>
#endif

#define MIN(x,y)	((x>y)?y:x)
#define MAX(x,y)	((x>y)?x:y)
#define ABS(x)		((x<0)?-x:x)

#define VNOTHING	0x00 /* desactivate vflag */
#define VGAME		0x02 /* during game */

extern volatile u8 vflag;

#endif //__INC_GENRES_SAMPLE__
