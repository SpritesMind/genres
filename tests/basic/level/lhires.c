#include <genesis.h>
#include <genres.h>

#include "utils/unpack.h"

#include "level.h"
#include "lhires.h"


extern	genresHires_t sfighter;

void LHires_Init( u16 firstTile )
{
	u16 tile2;
	u16 startx, starty;
	
	
	startx = ((VDP_getScreenWidth()/8) - sfighter.width) / 2;
	starty = (24 - sfighter.height) / 2;	
	
	VDP_setPalette( PAL1, sfighter.pal1 );
	VDP_setPalette( PAL2, sfighter.pal2 );
	
	if ( sfighter.compressedSize1 )
		unpackRLE( sfighter.tiles1, GFX_WRITE_VRAM_ADDR( 32*firstTile ), sfighter.compressedSize1);
	else
		VDP_doVRamDMA( (u32) sfighter.tiles1, 32*firstTile, sfighter.height*sfighter.width*32/2 ); //size in word
	
	tile2 = firstTile + sfighter.width*sfighter.height;
	if ( sfighter.compressedSize2 )
		unpackRLE( sfighter.tiles2, GFX_WRITE_VRAM_ADDR( 32*tile2 ), sfighter.compressedSize2);
	else
		VDP_doVRamDMA( (u32) sfighter.tiles2, 32*tile2, sfighter.height*sfighter.width*32/2 ); //size in word
		
	VDP_fillTileMapRectInc(VDP_PLAN_A, TILE_ATTR_FULL(PAL1, 0, 0, 0, firstTile) , startx, starty, sfighter.width, sfighter.height);
	VDP_fillTileMapRectInc(VDP_PLAN_B, TILE_ATTR_FULL(PAL2, 0, 0, 0, tile2) , startx, starty, sfighter.width, sfighter.height);



	Level_Text( 1, 23, "HIRES SAMPLE" );
}

void LHires_Update( )
{
	if ( JOY_readJoypad(JOY_1) & BUTTON_A )
	{	
		Level_Init( LEVEL_SPRITE, 1 );
	}
}
