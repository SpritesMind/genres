#ifndef __INC_LEVEL__
#define __INC_LEVEL__

#define	LEVEL_LOGO		0x00
#define	LEVEL_BITMAP	(LEVEL_LOGO+1)
#define	LEVEL_HIRES		(LEVEL_BITMAP+1)
#define LEVEL_SPRITE	(LEVEL_HIRES+1)
#define LEVEL_SHEET		(LEVEL_SPRITE+1)
#define LEVEL_MAP		(LEVEL_SHEET+1)
#define LEVEL_MAP2		(LEVEL_MAP+1)
#define LEVEL_DAC		(LEVEL_MAP2+1)
#define LEVEL_PSG		(LEVEL_DAC+1)
#define LEVEL_ECHO		(LEVEL_PSG+1)
#define LEVEL_ECHOMIDI	(LEVEL_ECHO+1)
#define LEVEL_CONTROL	(LEVEL_ECHOMIDI+1)


struct str_Level{
	u8	idx;
	u8	width;
	u8	height;
	u8 	pal;
	u16	data;
};
typedef struct str_Level level_t;

extern level_t curLevel;

void Level_Init( u8 levelNum , u16 firstTile );
void Level_Update(  );
void Level_Text( u8 x, u8 y, char *sText );

#endif
