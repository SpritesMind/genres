#include <genesis.h>
#include <genres.h>

#include "level.h"
#include "logo.h"


extern u32 *myFont;
extern u32 raw_size;

void Logo_Init( )
{
	char intChar[10];

	intToStr(raw_size,intChar,10);

	VDP_loadFontData( (u32 *) &myFont, FONT_LEN, TRUE);

	Level_Text( 13, 10, "TOUR DE GENRES" );

	Level_Text(  2, 26, "font loaded" );
	Level_Text(  2, 27, "data (in bytes) :" );
	Level_Text( 20, 27,  intChar);
}

void Logo_Update( )
{
	if ( JOY_readJoypad(JOY_1) & BUTTON_A )
	{	
		Level_Init( LEVEL_BITMAP, 1 );
	}
}
