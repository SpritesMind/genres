#include <genesis.h>
#include <genres.h>

#include "utils/unpack.h"
#include "utils/echo.h"

#include "level.h"
#include "lecho.h"

extern const void* const instruments[1];

extern const u8 Midnas[];
extern const u8 Hol[];
extern const u8 Minion[];
extern const u8 Nelpel[];
extern const u8 Megajysays[];
extern const u8 Doomsday[];
extern const u8 PianoTest[];
extern const u8 SquSawTest2[];
extern const u8 SquSawTest1[];
extern const u8 PSGTest[];
extern const u8 DrumTest[];
extern const u8 FluteTest[];

const u8* const BGMList[] = {
		Midnas,
		Hol,
		Minion,
		Nelpel,
		Megajysays,
		Doomsday,
		PianoTest,
		SquSawTest2,
		SquSawTest1,
		PSGTest,
		DrumTest,
		FluteTest,
		NULL
    };
extern const u8 SFX_Test[1];
extern const u8 SFX_Beep[1];

static u8 songIndex;
static u8 done;

static void printSongIndex( )
{
	char strIdx[3];

	intToStr(songIndex, strIdx, 2);
	Level_Text(1, 24, "playing song" );
	Level_Text(14, 24, strIdx );
}

static void playSong( )
{
	echo_stop_bgm( ); //needed ?

	echo_play_bgm( BGMList[songIndex] );
	printSongIndex();
}

static void handleJoy(u16 joy, u16 changed, u16 state)
{
	u16 key = changed & state;
	if (key)
	{
		switch(key)
		{
			case BUTTON_A:
				echo_stop_bgm();
				echo_stop_sfx();

				done = TRUE;
				break;
			case BUTTON_B:
				echo_stop_bgm();
				break;
			case BUTTON_C:
				echo_resume_bgm();
				break;
			case BUTTON_LEFT:
				if (songIndex > 0)
				{
					songIndex--;
					playSong();
				}
				break;
			case BUTTON_RIGHT:
				if (BGMList[songIndex+1] != NULL)
				{
					songIndex++;
					playSong();
				}
				break;
			case BUTTON_UP:
				echo_play_sfx(SFX_Test);
				break;
			case BUTTON_DOWN:
				echo_play_sfx(SFX_Beep);
				break;
		}
	}
}


void LEcho_Init( u16 firstTile )
{
	done = FALSE;

	Level_Text(7, 10, "B             stop song" );
	Level_Text(7, 11, "C             resume song" );
	Level_Text(7, 12, "Left/Right    select song" );
	Level_Text(7, 14, "Up            play long SFX");
	Level_Text(7, 15, "Down          play short SFX");

	Level_Text(1, 23, "ECHO SAMPLE (native demos from Sik)" );

	echo_loadDriver();
	echo_init( (const void **) instruments );

	songIndex = 0;
	playSong( );

	JOY_setEventHandler(handleJoy);
}

void LEcho_Update( )
{
	if (done)
	{
		Level_Init( LEVEL_ECHOMIDI, 1 );
	}
}
