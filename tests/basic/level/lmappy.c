#include <genesis.h>

#include <utils/unpack.h>
#include <utils/mappy.h>

#include "level.h"
#include "lmappy.h"

#define MAX_SCROLLV	(8*32)
#define MAX_SCROLLH (8*64)

extern genresMap_t cybernoid;

struct mappyLevel 		foreground;
struct mappyLevel 		background;


void LMappy_Init( u16 firstTile )
{
	VDP_setPlanSize(64,32);

	background.data = &cybernoid;
	background.idxPal = PAL1;
	background.idxTile = firstTile;
	background.idxLevel = 0;
	background.scrollH = 0;
	background.scrollV = 0;
	background.fieldAdr = BPLAN;

	foreground.data = &cybernoid;
	foreground.idxPal = PAL1;
	foreground.idxTile = firstTile;
	foreground.idxLevel = 1;
	foreground.scrollH = 0;
	foreground.scrollV = 0;
	foreground.fieldAdr = APLAN;
	
	mappy_Load(&background);
	
	mappy_Draw(&background);
	mappy_Draw(&foreground);

	Level_Text( 1, 23, "MAPPY SAMPLE" );
	Level_Text( 1, 24, "DIR   scroll FG" );
	Level_Text( 1, 25, "DIR+B scroll FG faster" );
	Level_Text( 1, 26, "DIR+C scroll FG fastest" );
}

void LMappy_Update( )
{
	u16 key = JOY_readJoypad(JOY_1);
	u8 speed = 1;
	if (key & BUTTON_B)
		speed= 4;
	else if (key & BUTTON_C)
		speed= 8;
	else if (key & BUTTON_A)
	{	
		VDP_setHorizontalScroll(PLAN_A, 0);
		VDP_setVerticalScroll(PLAN_A,  0);

		Level_Init( LEVEL_MAP2, 1 );
		return;
	}

	if (key&BUTTON_LEFT)
		mappy_ScrollH( &foreground, -1*speed);
	else if ( key&BUTTON_RIGHT )
		mappy_ScrollH( &foreground, 1*speed);

	if (key&BUTTON_UP)
		mappy_ScrollV( &foreground, -1*speed);
	else if ( (key&BUTTON_DOWN))
		mappy_ScrollV( &foreground, 1*speed);
	

	mappy_Update(&foreground);

	VDP_setHorizontalScroll(PLAN_A, -foreground.scrollH%MAX_SCROLLH);
	VDP_setVerticalScroll(PLAN_A, foreground.scrollV%MAX_SCROLLV);
}
