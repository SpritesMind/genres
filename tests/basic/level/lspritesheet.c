#include <genesis.h>
#include <genres.h>


#include "level.h"
#include "lspritesheet.h"


extern genresSpritesheet_t bjack_sheet;

u8 animIdx;
u8 frameCounter;
u8 frameIdx;
u16 tileIdx;

static void launchAnim( )
{
	frameCounter = bjack_sheet.animations[animIdx]->frameRate;
	frameIdx = 0;

	VDP_loadTileData( (u32 *) bjack_sheet.frames[  bjack_sheet.animations[animIdx]->frames[frameIdx] ], tileIdx,  bjack_sheet.frameTiles, TRUE );
}

static void updateAnim()
{
	frameCounter--;
	if (frameCounter == 0)
	{
		frameCounter = bjack_sheet.animations[animIdx]->frameRate;

		frameIdx++;
		if (frameIdx >= bjack_sheet.animations[animIdx]->frameCount )
			frameIdx = 0;

		VDP_loadTileData( (u32 *) bjack_sheet.frames[  bjack_sheet.animations[animIdx]->frames[frameIdx] ], tileIdx,  bjack_sheet.frameTiles, TRUE );
	}
}

static void handleJoy(u16 joy, u16 changed, u16 state)
{
	if (joy == JOY_1)
	{
		if (changed & state) //pressed
		{
			if (changed == BUTTON_LEFT)
			{
				if ( animIdx > 0 )
				{
					animIdx--;
					launchAnim();
				}
			}
			else if (changed == BUTTON_RIGHT)
			{
				if ( animIdx < (bjack_sheet.nbAnimations-1) )
				{
					animIdx++;
					launchAnim();
				}
			}
		}
	}
}

void LSpritesheet_Init( u16 firstTile )
{
	u8 i, j;
	u16 nbSprites = bjack_sheet.frameSpriteH*bjack_sheet.frameSpriteV;
	u16 spriteIdx = 0;

	u8 spriteWidth = ((bjack_sheet.spriteSize&0xF)+1)<<3;
	u8 spriteHeight = ( ((bjack_sheet.spriteSize&0xF0)>>2)+1)<<3;
	u8 spriteTiles = ((bjack_sheet.spriteSize&0xF)+1) * (((bjack_sheet.spriteSize&0xF0)>>2)+1);
	
	VDP_setPalette( PAL1, bjack_sheet.pal);



	for (i=0; i< bjack_sheet.frameSpriteH; i++)
	{
		for (j=0; j< bjack_sheet.frameSpriteV; j++)
		{
			VDP_setSprite(spriteIdx,
					100 + spriteWidth*i,
					100 + spriteHeight*j,
					bjack_sheet.spriteSize,
					TILE_ATTR_FULL(PAL1, 0,0,0, (firstTile + spriteIdx*spriteTiles)),
					((spriteIdx==(nbSprites-1))?0:spriteIdx+1) );
			spriteIdx++;
		}
	}
	
	Level_Text( 1, 23, "SPRITESHEET SAMPLE (use left/right)" );
	Level_Text( 1, 24, "replace ANI support" );

	tileIdx = firstTile;

	launchAnim( );

	JOY_setEventHandler(handleJoy);
}

void LSpritesheet_Update( )
{
	if ( JOY_readJoypad(JOY_1) & BUTTON_A )
	{	
		Level_Init( LEVEL_MAP, 1 );
		return;
	}



	updateAnim();
}
