#include <genesis.h>
#include <genres.h>

#ifdef DEBUG
#include <kdebug.h>
#endif

#include "level.h"
#include "lcontrols.h"

#define THRESHOLD	100 //2sec

volatile u8 ok;
volatile u8 threshold;

static u8 idx;
const genresControlSequence_t *combo;

extern genresControl_t controlKey;

static u8 checkKeySequence(u16 joy, u16 buttons_status)
{
	u16 key = combo->keys[idx];
#ifdef DEBUG
			KDebug_Alert("Testing");
			KDebug_AlertNumber(buttons_status);
			KDebug_Alert("vs");
			KDebug_AlertNumber(key);
#endif

	if (key == buttons_status)
	{
#ifdef DEBUG
			KDebug_Alert("Good one !");
#endif
		idx++;
		if (combo->keys[idx] == 0)	return TRUE;

	}
	else
	{
#ifdef DEBUG
			KDebug_Alert("Not the good one");
#endif
		idx = 0;
	}
	return FALSE;
}

static void handleJoy(u16 joy, u16 changed, u16 state)
{
	if (joy == JOY_1)
	{
		if (changed & state) //pressed
		{
			threshold = THRESHOLD;

			ok = checkKeySequence(joy, state & BUTTON_ALL);
		}
	}
}

void LControls_Init( )
{
	ok = FALSE;
	idx = 0;
	threshold = THRESHOLD;
	combo = controlKey.sequences[0];

	Level_Text( 4, 12, "KONAMI CODE NEEDED TO CONTINUE" );

	JOY_setEventHandler(handleJoy);
}

void LControls_Update( )
{
	if (ok)
	{	
		Level_Init( LEVEL_BITMAP, 1 );
	}
	else
	{
		if (--threshold == 0 )
		{
#ifdef DEBUG
			KDebug_Alert("Too late");
#endif
			idx = 0;
			threshold = THRESHOLD;
		}
	}
}
