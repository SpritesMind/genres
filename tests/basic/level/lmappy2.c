#include <genesis.h>

#include <utils/unpack.h>
#include <utils/mappy.h>

#include "level.h"
#include "lmappy2.h"

#define MAX_SCROLLV	(8*32)
#define MAX_SCROLLH (8*64)

extern genresMap_t map2;

struct mappyLevel 		foreground;
struct mappyLevel 		background;


void LMappy2_Init( u16 firstTile )
{
	VDP_setPlanSize(64,32);

	background.data = &map2;
	background.idxPal = PAL1;
	background.idxTile = firstTile;
	background.idxLevel = 0;
	background.scrollH = 0;
	background.scrollV = 0;
	background.fieldAdr = BPLAN;

	foreground.data = &map2;
	foreground.idxPal = PAL1;
	foreground.idxTile = firstTile;
	foreground.idxLevel = 1;
	foreground.scrollH = 0;
	foreground.scrollV = 0;
	foreground.fieldAdr = APLAN;
	
	mappy_Load(&background);
	
	mappy_Draw(&background);
	mappy_Draw(&foreground);

	Level_Text( 1, 23, "MAPPY FORCE16 TILE SAMPLE" );
}

void LMappy2_Update( )
{
	u16 key = JOY_readJoypad(JOY_1);
	if (key & BUTTON_A)
	{	
		Level_Init( LEVEL_DAC, 1 );
		return;
	}

//	mappy_Update(&foreground);
}
