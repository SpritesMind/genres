#include <genesis.h>
#include "genres_sample.h"

#include <utils/unpack.h>

#include "level.h"
#include "logo.h"
#include "lbitmap.h"
#include "lhires.h"
#include "lsprite.h"
#include "lspritesheet.h"
#include "lmappy.h"
#include "lmappy2.h"
#include "ldac.h"
#include "lpsg.h"
#include "lecho_midi.h"
#include "lecho.h"
#include "lcontrols.h"

level_t curLevel;

/////// draw a text starting at pos x,y with PAL0
void Level_Text( u8 x, u8 y, char *sText )
{
	VDP_drawTextBG(VDP_PLAN_B, sText, TILE_ATTR(PAL0,0,0,0), x, y);
}

void Level_Init( u8 levelNum, u16 firstTile )
{
	volatile u16 tileIdx;

	vflag = VNOTHING;

	VDP_resetSprites();
	VDP_updateSprites();
	VDP_resetScreen();
	JOY_init();
	VDP_waitVSync();
	
	// def value
	curLevel.idx = levelNum;
	curLevel.width = 64;
	curLevel.height = 32;
	curLevel.pal = PAL0;
	curLevel.data = 0;

	VDP_setPalette(PAL0, palette_grey);
	tileIdx = TILE_USERINDEX;// + 66;
	
	switch( levelNum )
	{
		case LEVEL_LOGO:	 //draw logo
			Logo_Init( );
			break;

		case LEVEL_BITMAP:
			LBitmap_Init( tileIdx );
			break;		

		case LEVEL_HIRES:
			LHires_Init( tileIdx );
			break;		
			
		case LEVEL_SPRITE:
			LSprite_Init( tileIdx );
			break;		

		case LEVEL_SHEET:
			LSpritesheet_Init( tileIdx );
			break;
			
		case LEVEL_MAP:
			LMappy_Init( tileIdx );
			break;

		case LEVEL_MAP2:
			LMappy2_Init( tileIdx );
			break;

		case LEVEL_DAC:
			LDAC_Init( tileIdx );
			break;

		case LEVEL_PSG:
			LPSG_Init( tileIdx );
			break;

		case LEVEL_ECHO:
			LEcho_Init( tileIdx );
			break;

		case LEVEL_ECHOMIDI:
			LEchoMIDI_Init( tileIdx );
			break;

		case LEVEL_CONTROL:
			LControls_Init( );
			break;
	}

	vflag = VGAME;
}

/****** LEVEL_UPDATE ****************
 * update the current level
 *************************************/
void Level_Update(  )
{
	switch( curLevel.idx )
	{
		case LEVEL_LOGO:			
			Logo_Update( );
			break;

		case LEVEL_BITMAP:
			LBitmap_Update( );
			break;		
		
		case LEVEL_HIRES:
			LHires_Update( );
			break;		
		
		case LEVEL_SPRITE:
			LSprite_Update( );
			break;

		case LEVEL_SHEET:
			LSpritesheet_Update( );
			break;

		case LEVEL_MAP:
			LMappy_Update( );
			break;
			
		case LEVEL_MAP2:
			LMappy2_Update( );
			break;

		case LEVEL_DAC:
			LDAC_Update( );
			break;

		case LEVEL_PSG:
			LPSG_Update( );
			break;

		case LEVEL_ECHO:
			LEcho_Update( );
			break;

		case LEVEL_ECHOMIDI:
			LEchoMIDI_Update( );
			break;

		case LEVEL_CONTROL:
			LControls_Update( );
			break;
	}
	
	VDP_updateSprites();

}
