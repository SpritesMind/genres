#include <genesis.h>
#include <genres.h>
#include "utils/echo.h"
#include "level.h"
#include "lpsg.h"

#ifdef DEBUG
#include <kdebug.h>
#endif

#define PSG_COUNT	5


//lost with pointer to array ? 
//welcome aboard!
//try http://stackoverflow.com/a/859650 ;)


extern const genresPSGFrame_t arcanoid2_1[1];
extern const u32 arcanoid2_1_duration;

extern const genresPSGFrame_t psg_down[1];
extern const u32 psg_down_duration;

extern const genresPSGFrame_t psg_single[1];
extern const u32 psg_single_duration;
extern const genresPSGFrame_t psg_single2[1];
extern const u32 psg_single2_duration;

extern const genresPSGFrame_t sw[1];
extern const u32 sw_duration;

static u8 psgIndex;
static u8 done;

static u8 isPlaying;
static const genresPSGFrame_t (*psgData)[1];
static u32 frameIdx;

#define MODE_SONG	0
#define MODE_TONE	1

static u8 mode;
static u8 loop;
static u16 psgTone;
static genresPSGFrame_t toneFrame;

struct PSGInfo
{
	char *name;
	const u32 *nbFrames;
	const genresPSGFrame_t (*data)[1];	
}psg[PSG_COUNT] = {
	{
		"AYFX Low          ",
		&psg_single_duration,
		&psg_single
	},
	{
		"AYFX High         ",
		&psg_single2_duration,
		&psg_single2
	},
	{
		"AYFX Low to High  ",
		&psg_down_duration,
		&psg_down
	},
	{
		"AYFX Arcanoid     ",
		&arcanoid2_1_duration,
		&arcanoid2_1
	},
	{
		"YM StarWars CPC   ",
		&sw_duration,
		&sw
	}
};




static void printPSGName( )
{
	mode = MODE_SONG;
	Level_Text(10, 24, psg[psgIndex].name );
}

//// quick 68000-side PSG driver
static void resetPSG()
{	
	PSG_init();
	
	psgData = NULL;
	frameIdx = 0;
	loop = 0;
	isPlaying = FALSE;
	
}
static void updatePSG()
{
	genresPSGFrame_t *frame;
	u8 chan;
	
	frame = &toneFrame;
	
	if (mode == MODE_SONG)
	{
		frame = (genresPSGFrame_t *)psgData[frameIdx];
		//TODO : print frameidx/frame->nbFrames
		if (loop == 0)	loop = (frame->mixer >>4); //loop 1, mean 1 play, so loop-- at first
		if (loop)	loop--; 
		if (loop == 0)	frameIdx++;				
	}	
	if (!isPlaying)	return;
	
	if ( frame->mixer == 0 )
	{
		//eof
		resetPSG();
		return;
	}

	if (frame->mixer & 0x08)
	{
		//TODO
		//PSG_write(frame->noise);
	}
	else
	{
		//TODO
		//PSG_setNoise(0,0);
	}	
	
	for (chan = 0; chan<3; chan++)
	{
		if (frame->mixer & (1<<chan))
		{
			PSG_write( frame->channels[chan].volume )  ;
			PSG_write( frame->channels[chan].toneLatch );
			PSG_write( frame->channels[chan].toneData  );
		}
		else
		{
			PSG_setTone(chan, 0);
			PSG_setEnvelope(chan, 0xF); //silent		
		}
	}
	
}
static void stopPSG()
{
	resetPSG();
}
static void playPSG()
{	
	if (mode == MODE_TONE)
		return;

	resetPSG();

	
	if (psg[psgIndex].nbFrames == 0)	return;
	if (psg[psgIndex].data == NULL)		return;

	psgData = psg[psgIndex].data;
	
	isPlaying = TRUE;
}

static void playPSGTone(u8 channel)
{
	u8 toneValue[9];
	toneFrame.mixer = 0xF1;
	toneFrame.noise = 0;
	toneFrame.channels[0].volume = ( 0x80 | (channel << 5) | 0x10 | 8 );
	toneFrame.channels[0].toneLatch = ( 0x80 | (channel << 5) | 0x00 | (psgTone&0xF) );
	toneFrame.channels[0].toneData = ( 0x00 | (psgTone >> 4) );

	Level_Text(10, 24, "TONE              " );
	intToStr(psgTone, toneValue, 5);
	Level_Text(15, 24, toneValue );
	
	mode = MODE_TONE;
	
	isPlaying = TRUE;
}


static void handleJoy(u16 joy, u16 changed, u16 state)
{
	u16 key = changed & state;
	if (key)
	{
		switch(key)
		{
			case BUTTON_A:
				stopPSG();

				done = TRUE;
				break;
			case BUTTON_B:
				playPSG();
				break;
			case BUTTON_C:
				stopPSG();
				break;
			case BUTTON_LEFT:
				if (psgIndex > 0)
				{
					stopPSG();
					psgIndex--;
					printPSGName( );
				}
				break;
			case BUTTON_RIGHT:
				if (psgIndex < PSG_COUNT-1)
				{
					stopPSG();
					psgIndex++;
					printPSGName( );
				}
				break;
			case BUTTON_UP:
				if (psgTone > 0)
				{
					stopPSG();		
					psgTone--;
					playPSGTone(0);
				}
				break;
			case BUTTON_DOWN:
				if (psgTone < 0x3FF)
				{
					stopPSG();	
					psgTone++;
					playPSGTone(0);
				}
				break;
		}
	}
}


void LPSG_Init( u16 firstTile )
{
	done = FALSE;

	Level_Text(7, 10, "B             play sound" );
	Level_Text(7, 11, "C             reset" );
	Level_Text(7, 12, "Left/Right    select song" );
	Level_Text(7, 13, "Up/Down       select tone" );

	Level_Text(1, 23, "PSG SAMPLE" );
	
	psgTone = 0;
	
	
	psgIndex = 0;
	printPSGName( ); //song mode

	JOY_setEventHandler(handleJoy);
}

void LPSG_Update( )
{
	updatePSG();
	
	if (done)
	{
		//already done on pressA
		//stopSound();
		Level_Init( LEVEL_ECHO, 1 );
	}
}
