#include <genesis.h>
#include <genres.h>

#include "utils/unpack.h"
#include "utils/echo.h"

#include "level.h"
#include "lecho_midi.h"



extern const void* const sample_instruments[1];
extern const u8 drum_sample[];
extern const u8 drum_sample2[];
extern const u8 bass_sample[];
extern const u8 bass_sample2[];
extern const u8 midi_sample[];

/*
extern const void* const u2_instruments[1];
extern const u8 u2_song[];

*/


static u8 songIndex;
static u8 done;
static u8 paused;

static void printSongIndex( )
{
	switch (songIndex)
	{
		case 0:
			Level_Text(1, 24, "playing Drum Sample             " );
			break;
		case 1:
			Level_Text(1, 24, "playing Drum Sample 2           " );
			break;
		case 2:
			Level_Text(1, 24, "playing Bass Sample             " );
			break;
		case 3:
			Level_Text(1, 24, "playing Bass Sample 2           " );
			break;
		case 4:
			Level_Text(1, 24, "playing MIDI Sample             " );
			break;
		case 5:
			Level_Text(1, 24, "playing With or without you - U2" );
			break;
	}
}

static void playSong( )
{
	echo_stop_bgm( );

	paused = FALSE;

	switch (songIndex)
	{
			case 0:
				echo_init( (const void **) sample_instruments );
				echo_play_bgm( drum_sample );
				break;
			case 1:
				//echo_init( (const void **) sample_instruments );
				echo_play_bgm( drum_sample2 );
				break;
			case 2:
				//echo_init( (const void **) sample_instruments );
				echo_play_bgm( bass_sample );
				break;
			case 3:
				//echo_init( (const void **) sample_instruments );
				echo_play_bgm( bass_sample2 );
				break;
			case 4:
				//echo_init( (const void **) sample_instruments );
				echo_play_bgm( midi_sample );
				break;
				/*
			case 5:
				echo_init( (const void **) u2_instruments );
				echo_play_bgm( u2_song );
				break;*/
			default:
				break;
	}

	printSongIndex();
}

static void handleJoy(u16 joy, u16 changed, u16 state)
{
	u16 key = changed & state;
	if (key)
	{
		switch(key)
		{
			case BUTTON_A:
				echo_stop_bgm();
				done = TRUE;
				break;
			case BUTTON_B:
				paused = !paused;
				if (paused)
					echo_stop_bgm();
				else
					echo_resume_bgm();
				break;
			case BUTTON_C:
				playSong( );
				break;
			case BUTTON_LEFT:
				if (songIndex > 0)
				{
					songIndex--;
					playSong();
				}
				break;
			case BUTTON_RIGHT:
				if (songIndex < 5)
				{
					songIndex++;
					playSong();
				}
				break;
		}
	}
}


void LEchoMIDI_Init( u16 firstTile )
{
	done = FALSE;


	Level_Text(7, 10, "B             pause song" );
	Level_Text(7, 11, "C             reset song" );
	Level_Text(7, 12, "Left/Right    select song" );

	//Level_Text(7, 14, "Up            play long SFX");
	//Level_Text(7, 15, "Down          play short SFX");


	Level_Text(1, 23, "ECHO SAMPLE (MIDI)" );

	echo_loadDriver();

	songIndex = 0;
	playSong( );

	JOY_setEventHandler(handleJoy);
}

void LEchoMIDI_Update( )
{
	if (done)
	{
		Level_Init( LEVEL_CONTROL, 1 );
	}
}
