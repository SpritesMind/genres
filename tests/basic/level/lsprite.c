#include <genesis.h>
#include <genres.h>

#include "level.h"
#include "lsprite.h"


extern genresSprites_t bjack;

void LSprite_Init( u16 firstTile )
{
	u8 i;
	
	VDP_setPalette( PAL1, bjack.pal);

	for (i=0; i< 17; i++)
	{
		VDP_setSprite(i, (i/4)*50, (i%4)*50, bjack.size, TILE_ATTR_FULL(PAL1, 0,0,0, (firstTile + i*(bjack.height/8)*(bjack.width/8))), (i==16?0:i+1) );
	}
	
	Level_Text( 1, 23, "SPRITE SAMPLE" );

	VDP_loadTileData( bjack.sprites[24], firstTile, 17 * (bjack.height/8) * (bjack.width/8), TRUE );
}

void LSprite_Update( )
{
	if ( JOY_readJoypad(JOY_1) & BUTTON_A )
	{	
		Level_Init( LEVEL_SHEET, 1 );
	}
}
