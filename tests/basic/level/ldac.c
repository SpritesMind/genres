#include <genesis.h>
#include <genres.h>
#include "utils/echo.h"
#include "level.h"
#include "ldac.h"

#define		SND_FORMAT_PCM		1 //8 bit signed from 8 Khz up to 32 Khz
#define		SND_FORMAT_2ADPCM	2 //4 bits ADPCM at 22050 Hz
#define		SND_FORMAT_4PCM		3 //8 bit signed at 16 Khz, 256 bytes boundary (address and size)
#define		SND_FORMAT_MVS		4 //8 bit unsigned at 9346Hz, max 65536 byte
#define		SND_FORMAT_VGM		5 //8 bit signed (?), max 8Khz (?!)
#define		SND_FORMAT_EWF		6 //8 bits unsigned, at 10650Hz mono, with 0xFE highest possible value
#define		SND_FORMAT_XGM		7 //8 bits unsigned, at 14KHz


const char* driverNames[] =
{
"PCM   ",
"2ADPCM",
"4PCM  ",
"MVS   ",
"VGM   ",
"EWF   ",
"XGM   "
};


extern const u8 sega_pcm[1];
extern const u32 sega_pcm_size;

extern const u8 sega_adpcm[1];
extern const u32 sega_adpcm_size;

extern const u8 sega_4pcm[1];
extern const u32 sega_4pcm_size;

extern const u8 sega_mvs[1];
extern const u32 sega_mvs_size;

extern const u8 sega_vgm[1];
extern const u32 sega_vgm_size;
extern const u8 sonic1[];

extern const void* const instruments_pcm[1];
//extern const u8 sega_ewf[1];
//extern const u32 sega_ewf_size;
extern const u8 sega_esf[];

extern const u8 sega_xgm[1];
extern const u32 sega_xgm_size;



static u8 driverIndex;
static u8 done;

static inline void printDriverName( )
{
	Level_Text(10, 24, (char *)driverNames[driverIndex-1] );
}

static void loadDriver()
{
	printDriverName( );

	switch( driverIndex )
		{
			case SND_FORMAT_PCM:
			case SND_FORMAT_2ADPCM:
			case SND_FORMAT_4PCM:
			case SND_FORMAT_MVS:
				//handled by SGDK's sound.c
				break;
			case SND_FORMAT_VGM:
				SND_startPlay_VGM(sonic1);
				break;
			case SND_FORMAT_EWF:
				echo_loadDriver();
				echo_init( (const void **) instruments_pcm );
				break;
			case SND_FORMAT_XGM:
				SND_setPCM_XGM(64, sega_xgm, sega_xgm_size);
				break;
		}
}

static void playSound( )
{
	switch( driverIndex )
	{
		case SND_FORMAT_PCM:
			SND_startPlay_PCM(sega_pcm, sega_pcm_size, SOUND_RATE_32000, SOUND_PAN_CENTER, FALSE);
			break;
		case SND_FORMAT_2ADPCM:
			SND_startPlay_2ADPCM(sega_adpcm, sega_adpcm_size, SOUND_PCM_CH1, FALSE);
			break;
		case SND_FORMAT_4PCM:
			SND_startPlay_4PCM_ENV(sega_4pcm, sega_4pcm_size, SOUND_PCM_CH1, FALSE);
			break;
		case SND_FORMAT_MVS:
			SND_startDAC_MVS(sega_mvs, sega_mvs_size);
			break;
		case SND_FORMAT_VGM:
			SND_playSfx_VGM(sega_vgm, sega_vgm_size);
			break;
		case SND_FORMAT_EWF:
			echo_play_sfx(sega_esf);
			break;
		case SND_FORMAT_XGM:
			SND_startPlayPCM_XGM(64, 15, SOUND_PCM_CH1);
			break;
	}
}
static void stopSound( )
{
	switch( driverIndex )
		{
			case SND_FORMAT_PCM:
                SND_stopPlay_PCM();
				break;
			case SND_FORMAT_2ADPCM:
				SND_stopPlay_2ADPCM(SOUND_PCM_CH1);
				break;
			case SND_FORMAT_4PCM:
				SND_stopPlay_4PCM_ENV(SOUND_PCM_CH1);
				break;
			case SND_FORMAT_MVS:
				SND_stopDAC_MVS();
				break;
			case SND_FORMAT_VGM:
				SND_stopPlay_VGM();
				break;
			case SND_FORMAT_EWF:
				//echo_stop_sfx();
				break;
			case SND_FORMAT_XGM:
				SND_stopPlayPCM_XGM(SOUND_PCM_CH1);
				break;
		}
}

static void previousDriver( )
{
	if (driverIndex > SND_FORMAT_PCM)
	{
		stopSound();
		driverIndex--;
		loadDriver( );
	}
}

static void nextDriver( )
{
	if (driverIndex < SND_FORMAT_XGM)
	{
		stopSound();
		driverIndex++;
		loadDriver( );
	}
}

static void handleJoy(u16 joy, u16 changed, u16 state)
{
	u16 key = changed & state;
	if (key)
	{
		switch(key)
		{
			case BUTTON_A:
				stopSound();

				done = TRUE;
				break;
			case BUTTON_B:
				playSound();
				break;
			case BUTTON_C:
				stopSound();
				break;
			case BUTTON_LEFT:
				previousDriver( );
				break;
			case BUTTON_RIGHT:
				nextDriver( );
				break;
		}
	}
}


void LDAC_Init( u16 firstTile )
{
	done = FALSE;

	Level_Text(7, 10, "B             play SEGAAA" );
	Level_Text(7, 11, "C             stop it!" );
	Level_Text(7, 12, "Left/Right    select driver" );
	//Level_Text(7, 14, "Up            play long SFX");
	//Level_Text(7, 15, "Down          play short SFX");

	Level_Text(1, 23, "DAC SAMPLE (for supported drivers)" );

	driverIndex = SND_FORMAT_PCM;

	loadDriver();
	JOY_setEventHandler(handleJoy);
}

void LDAC_Update( )
{
	if (done)
	{
		//already done on pressA
		//stopSound();
		Level_Init( LEVEL_PSG, 1 );
	}
}
