#include <genesis.h>
#include <genres.h>

#include "utils/unpack.h"

#include "level.h"
#include "lbitmap.h"


extern	genresTiles_t spritesmind;
extern	genresPal_t spritesmind_pal;

void LBitmap_Init( u16 firstTile )
{
	//u16 posy;
	u16 startx, starty;
	
	startx = ((VDP_getScreenWidth()/8) - spritesmind.width) / 2;
	starty = (24 - spritesmind.height) / 2;	
	
	VDP_setPalette( PAL1, spritesmind_pal.colors );
	
	if ( spritesmind.compressedSize )
		unpackRLE( spritesmind.tiles, GFX_WRITE_VRAM_ADDR( 32*firstTile ), spritesmind.compressedSize);
	else
		VDP_doVRamDMA( (u32) spritesmind.tiles, 32*firstTile, spritesmind.height*spritesmind.width*32/2 ); //size in word
		
	VDP_fillTileMapRectInc(VDP_PLAN_A, TILE_ATTR_FULL(PAL1, 0, 0, 0, firstTile) , startx, starty, spritesmind.width, spritesmind.height);

	/*
	for (posy=0; posy < spritesmind.height; posy++)
	{
		VDP_fillTileData()
		Video_Fill( firstTile + posy*spritesmind.width, firstTile + (posy+1)*spritesmind.width -1, PAL1, startx, posy+starty, APLAN, 0,0,0);
	}
	*/

	Level_Text( 1, 23, "BITMAP SAMPLE" );
}

void LBitmap_Update( )
{
	if ( JOY_readJoypad(JOY_1) & BUTTON_A )
	{	
		Level_Init( LEVEL_HIRES, 1 );
	}
}
