GenRes 2.1
===================
GenRes 2.1 is the new version of the advanced resource compiler for Sega Genesis.
While the output is mainly [SGDK](https://stephane-d.github.io/SGDK/) focused, you could use it with any other compiler / assembler.

*by KanedaFr (c) 2018 *[SpritesMind](http://gendev.spritesmind.net/forum.php)

Introduction
-------------

GenRes 1.0 was made for SGDK on January '08.
It mainly use the syntax `plug id file option1 option2 option3`.
With more and more options to suit everyone's needs, this syntax quickly became a nightmare to handle.
GenRes' goal was to make it easier for anyone to include resource, not to lost hours and hours trying to find the right syntax to add a sprite.

I so started to update GenRes with a more powerful parser to get rid of this awful syntax.

In the meantime, Stef, SGDK's author, made his own `rescomp`, which is now the official resource compiler for SGDK.
While it could make GenRes unneeded, I choose to update it but focused on my own needs. 
Since only a few people were using it, it wasn't a betrayal !

GenRes is not there to compete with `rescomp`, but an alternative with some interesting options.
But aware I won't supply support on this one, but you could try to drop a message on SpritesMind or my twitter accound ;)
----------

History
-------------
|Version|Date|Details|
|-|-|-|
| 2.0 | 1 dec 2015 | initial public release|
| 2.1 | 5 dec 2018 | Added HIRES plugin
Fixed MAP plugin
Added new MAP option FORCE16|


----------

Usage
-------------

    genres <source.rc> <export.asm> [-v]


source.rc is a file defining every resource using plugins available.
Every plugin follows the same syntax

    plugin id file
    {
    	option value
    	option value
    } 
> **Tip:** included `resource.rc` is a template you can use to create your own `source.rc`

Question ? Need support ? log in [SpritesMind forum](http://gendev.spritesmind.net/forum.php) !

----------

Plugins
-------------

> For history reason, I still use the word "plugin" to describe a resource converter/importer/compiler.

&nbsp;

> **Note:**
> - You won't find plugin syntax here : use `genres help <plugin name>` to get full syntax, help and sample
> - Format of generated data is defined on genres.h

#### BITMAP

Convert any image to tiles.
Use only the first 16 colors, so fix your palette first !


#### HIRES

Convert a 31 colors to 2 planes / 2 palettes data.
Give to your title or cut scene the quality they need!

#### FONT

minimal version of `BITMAP` to import font (96 tiles)

#### PAL

striped down version of `BITMAP` to import pal only

#### SPRITE

Like `BITMAP` but convert to suitable sprites

#### SPRITESHEET

A more advanced feature : convert a image with several sprites to a list of sprites, with framerate, user defined animation, collide box...

> **Tip:** A lot of spritesheets is available on the internet. Use them for testing !


#### MAP

Import layers and bitmap data from [Mappy](http://www.tilemap.co.uk/mappy.php) maps
Handle any size of tiles if 8-based (ie will split them in 8x8 tile)

#### CONTROLS

Add moves list or cheat code to your game.
Perfect for a fighting game !

#### DAC

Convert sound to use with any of the SGDK supported drivers

#### PSG

Import Atari, Amstrad, MSX.. tone *(no noise support)*

#### ECHO_BANK

Easily create Echo instrument list.
Support almost every ym2612 instrument dump format.

#### ECHO_BGM

Convert MIDI file to ECHO bgm

#### ECHO_SFX

Create sfx based on Echo instruments

#### DATA

Add raw data,  similar to bintoc tool or asm's INCBIN

----------

Samples
-------------

### Basic
This sample aims to explain how to use every kind of data supported by GenRes.
Be free to browse source code to learn how to use basis of GenRes

*Required* : SGDK with Genres2.1

----------

Third party code
-------------

GenRes, to handle maximum compatibility, uses third party code and lib:

 - [FreeImage](http://freeimage.sourceforge.net/) 
 - [Sound eXchange](http://sox.sourceforge.net/)
 - [ST-Sound Library](http://leonard.oxg.free.fr/StSoundGPL.html) by Arnaud Carré
 - Sik's Echo [mdtools](https://github.com/sikthehedgehog/mdtools)

----------

Developer
-------------

Want to clone, fork, whatever ?
To fix an issue or to add a plugin ?
To make your own &lt;insert your console name here&gt;Res ?
Go ahead !
No licence headache, do as you wish !
Just don't sell it, please.

For more info, please read `README.build` file and docs in lib folder.
